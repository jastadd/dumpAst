package de.tudresden.inf.st.jastadd.testDumper;

import de.tudresden.inf.st.jastadd.dumpAst.ast.DumpNode;
import de.tudresden.inf.st.jastadd.dumpAst.ast.TransformationException;
import org.jastadd.testDumper.ast.DropOffLocation;
import org.jastadd.testDumper.ast.Orientation;
import org.jastadd.testDumper.ast.Position;
import org.jastadd.testDumper.ast.Size;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static de.tudresden.inf.st.jastadd.testDumper.TestUtils.*;
import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Regression test cases.
 *
 * @author rschoene - Initial contribution
 */
public class TestRegression {

  @Test
  public void regressionIssue16() throws TransformationException {
    DropOffLocation location = new DropOffLocation();
    location.setName(ROOT_NAME);
    location.setPosition(new Position(T1_NAME, 1, 2, 3));
    location.setOrientation(new Orientation(T2_NAME, 4, 5, 6, 7));
    location.setSize(new Size(T3_NAME, 8, 9, 10));

    List<DumpNode> nodes = TestUtils.dumpModel(location);
    assertThat(valueTokens(findByName(nodes, T1_NAME))).containsOnly(
        entry("Name", T1_NAME),
        entry("X", 1.0),
        entry("Y", 2.0),
        entry("Z", 3.0));
  }

  @Test
  public void regressionNoNewlineForRelationStyleMustache() throws IOException {
    Path path = Paths.get("..", "dumpAst.base", "src", "main", "resources", "RelationStyle.mustache");
    List<String> lines = Files.readAllLines(path);
    assertEquals(1, lines.size(), "Ensure no trailing newline in RelationStyle.mustache");
  }
}
