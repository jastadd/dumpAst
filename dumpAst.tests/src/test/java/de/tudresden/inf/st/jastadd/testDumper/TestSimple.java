package de.tudresden.inf.st.jastadd.testDumper;

import de.tudresden.inf.st.jastadd.dumpAst.ast.DumpAst;
import de.tudresden.inf.st.jastadd.dumpAst.ast.DumpBuilder;
import de.tudresden.inf.st.jastadd.dumpAst.ast.DumpNode;
import de.tudresden.inf.st.jastadd.dumpAst.ast.TransformationException;
import org.jastadd.testDumper.ast.*;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

import static de.tudresden.inf.st.jastadd.testDumper.TestUtils.*;
import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.*;

public class TestSimple {

  @Test
  public void testEmpty() throws TransformationException {
    Root root = createRoot(null, null);

    List<DumpNode> nodes = TestUtils.dumpModel(root);
    assertThat(nodes).flatExtracting(NAME_EXTRACTOR).containsExactly(ROOT_NAME);
    DumpNode actualRoot = TestUtils.findByName(nodes, ROOT_NAME);
    assertEquals(1, actualRoot.getNumDumpToken());
    assertEmpty(actualRoot.getDumpChildNode(0));
    assertEquals(0, actualRoot.getNumDumpRelation());
  }

  @Test
  public void testCustomPreamble() throws IOException, TransformationException {
    Root root = createRoot(null, null);
    String preamble = "{<\n>\n}";
    ExposingDumpBuilder builder = new ExposingDumpBuilder(root);
    builder.excludeNullNodes().customPreamble(preamble);

    Path path = Paths.get("src/gen/resources/");
    Files.createDirectories(path);
    builder.dumpAsYaml(path.resolve("customPreamble.yaml"), true);

    DumpAst dumpAst = builder.build();
    String puml = dumpAst.toPlantUml();
    assertThat(puml).contains(preamble);
  }

  @Test void testChildlessNonterminal() throws TransformationException {
    Root root = createRoot(createA(A_NAME, a -> a.setD(new D())), null);
    List<DumpNode> nodes = TestUtils.dumpModel(root);
    Optional<DumpNode> actualD = nodes.stream().filter(n -> n.getObject() instanceof D).findFirst();
    assertTrue(actualD.isPresent());
    assertTrue(actualD.get().isAstNode());
  }

  @Test
  public void testOneNormalChild() throws TransformationException {
    Root root = createRoot(createA(A_NAME), null);

    List<DumpNode> nodes = TestUtils.dumpModel(root);
    assertThat(nodes).flatExtracting(NAME_EXTRACTOR).containsExactlyInAnyOrder(ROOT_NAME, A_NAME);
    DumpNode actualRoot = TestUtils.findByName(nodes, ROOT_NAME);
    assertEquals(1, actualRoot.getNumDumpToken());
    assertEquals(2, actualRoot.getNumDumpChildNode());
    assertEmpty(actualRoot.getDumpChildNode(1));
    assertEquals(0, actualRoot.getNumDumpRelation());
    assertThatMapOf(normalChildren(actualRoot)).containsExactlyInAnyOrder(tuple("A", A_NAME));
  }

  @Test
  public void testOneNormalChildIncludeNullNodes() throws TransformationException {
    Root root = createRoot(createA(A_NAME), null);

    List<DumpNode> nodes = TestUtils.dumpModel(root, DumpBuilder::includeNullNodes);
    assertThat(nodes).flatExtracting(NAME_EXTRACTOR).containsExactlyInAnyOrder(
        ROOT_NAME, A_NAME,
        null,  // C in Root
        null,  // B in A
        null,  // MyC in A
        null,  // D in A
        null,  // BiC2 in A
        null,  // BiC1 in A
        null   // BiC3 in A
    );
    DumpNode actualRoot = TestUtils.findByName(nodes, ROOT_NAME);
    assertEquals(1, actualRoot.getNumDumpToken());
    assertEquals(3, actualRoot.getNumDumpChildNode());
    assertEmpty(actualRoot.getDumpChildNode(1));
    assertEquals(0, actualRoot.getNumDumpRelation());

    DumpNode actualA = TestUtils.findByName(nodes, A_NAME);
    assertEquals(1, actualA.getNumDumpToken());
    assertEquals(3, actualA.getNumDumpChildNode());
    assertEquals(3, actualA.getNumDumpRelation());
  }

  @Test
  public void testListChildren() throws TransformationException {
    Root root = createRoot(null, null, createB(B1_NAME), createB(B2_NAME), createB(B3_NAME));

    List<DumpNode> nodes = TestUtils.dumpModel(root);
    assertThat(nodes).flatExtracting(NAME_EXTRACTOR).containsExactlyInAnyOrder(ROOT_NAME, B1_NAME, B2_NAME, B3_NAME);
    DumpNode actualRoot = TestUtils.findByName(nodes, ROOT_NAME);
    assertEquals(1, actualRoot.getNumDumpToken());
    assertEquals(1, actualRoot.getNumDumpChildNode());
    assertEquals(0, actualRoot.getNumDumpRelation());
    assertThatMapOf(listChildren(actualRoot), "B").containsExactlyInAnyOrder(B1_NAME, B2_NAME, B3_NAME);
  }

  @Test
  public void testOrderedListChildren() throws TransformationException {
    Root root = createRoot(createA(A_NAME), createC(C_NAME), createB(B1_NAME), createB(B2_NAME), createB(B3_NAME));

    List<DumpNode> nodes = TestUtils.dumpModel(root, DumpBuilder::orderChildren);
    assertThat(nodes).flatExtracting(NAME_EXTRACTOR).containsExactlyInAnyOrder(
        ROOT_NAME, A_NAME, B1_NAME, B2_NAME, B3_NAME, C_NAME);

    DumpNode actualRoot = TestUtils.findByName(nodes, ROOT_NAME);
    // in grammar: DumpAst ::= [...] DumpNode* [...];
    // use getParent() twice as first returns JastAddList for DumpNode list
    assertTrue(((DumpAst) actualRoot.getParent().getParent()).getPrintConfig().getOrderChildren());

    List<DumpNode> children = actualRoot.myChildren();
    assertThat(children).extracting(NAME_EXTRACTOR).containsExactly(
        A_NAME, B1_NAME, B2_NAME, B3_NAME, C_NAME);

    DumpNode actualA = TestUtils.findByName(nodes, A_NAME);
    DumpNode actualB1 = TestUtils.findByName(nodes, B1_NAME);
    DumpNode actualB2 = TestUtils.findByName(nodes, B2_NAME);
    DumpNode actualB3 = TestUtils.findByName(nodes, B3_NAME);
    DumpNode actualC = TestUtils.findByName(nodes, C_NAME);

    assertEquals(actualB1, actualA.successor(), actualA.successor().getName());
    assertEquals(actualB2, actualB1.successor(), actualB1.successor().getName());
    assertEquals(actualB3, actualB2.successor(), actualB2.successor().getName());
    assertEquals(actualC, actualB3.successor(), actualB3.successor().getName());
    assertNull(actualC.successor());

    assertTrue(actualA.hasSuccessor());
    assertTrue(actualB1.hasSuccessor());
    assertTrue(actualB2.hasSuccessor());
    assertTrue(actualB3.hasSuccessor());
    assertFalse(actualC.hasSuccessor());
  }

  @Test
  public void testNoOptChild() throws TransformationException {
    Root root = new Root().setName(ROOT_NAME);

    List<DumpNode> nodes = TestUtils.dumpModel(root);
    assertThat(nodes).flatExtracting(NAME_EXTRACTOR).containsExactlyInAnyOrder(ROOT_NAME);
    DumpNode actualRoot = TestUtils.findByName(nodes, ROOT_NAME);
    assertEquals(1, actualRoot.getNumDumpToken());
    assertEmpty(actualRoot.getDumpChildNode(0));
    assertEquals(0, actualRoot.getNumDumpRelation());
    assertThatMapOf(normalChildren(actualRoot)).isEmpty();
  }

  @Test
  public void testOneOptChild() throws TransformationException {
    Root root = createRoot(null, createC(C_NAME));

    List<DumpNode> nodes = TestUtils.dumpModel(root);
    assertThat(nodes).flatExtracting(NAME_EXTRACTOR).containsExactlyInAnyOrder(ROOT_NAME, C_NAME);
    DumpNode actualRoot = TestUtils.findByName(nodes, ROOT_NAME);
    assertEquals(1, actualRoot.getNumDumpToken());
    assertEquals(2, actualRoot.getNumDumpChildNode());
    assertEmpty(actualRoot.getDumpChildNode(0));
    assertEquals(0, actualRoot.getNumDumpRelation());
    assertThatMapOf(normalChildren(actualRoot)).containsExactlyInAnyOrder(tuple("C?", C_NAME));
  }

  @Test
  public void testNormalUniRelation() throws TransformationException {
    Root root = createRoot(createA(A_NAME), createC(C_NAME), createB(B_NAME));
    root.getB(0).setOneA(root.getA());

    List<DumpNode> nodes = TestUtils.dumpModel(root);
    assertThat(nodes).flatExtracting(NAME_EXTRACTOR).containsExactlyInAnyOrder(ROOT_NAME, A_NAME, B_NAME, C_NAME);
    DumpNode actualB = TestUtils.findByName(nodes, B_NAME);
    assertThatMapOf(normalRelationChildren(actualB)).containsExactlyInAnyOrder(tuple("OneA", A_NAME));
  }

  @Test
  public void testListUniRelation() throws TransformationException {
    final String A2_NAME = "anotherA";
    Root root = createRoot(createA(A_NAME), createC(C_NAME), createB(B_NAME));
    root.getC().setA(createA(A2_NAME));
    root.getB(0).addManyA(root.getA());
    root.getB(0).addManyA(root.getC().getA());

    List<DumpNode> nodes = TestUtils.dumpModel(root);
    assertThat(nodes).flatExtracting(NAME_EXTRACTOR).containsExactlyInAnyOrder(ROOT_NAME, A_NAME, A2_NAME, B_NAME, C_NAME);
    DumpNode actualB = TestUtils.findByName(nodes, B_NAME);
    assertThatMapOf(listRelationChildren(actualB), "ManyA").containsExactlyInAnyOrder(A_NAME, A2_NAME);
  }

  @Test
  public void testOptUniRelation() throws TransformationException {
    Root root = createRoot(createA(A_NAME), createC(C_NAME), createB(B_NAME));
    root.getB(0).setMaybeC(root.getC());

    List<DumpNode> nodes = TestUtils.dumpModel(root);
    assertThat(nodes).flatExtracting(NAME_EXTRACTOR).containsExactlyInAnyOrder(ROOT_NAME, A_NAME, B_NAME, C_NAME);
    DumpNode actualB = TestUtils.findByName(nodes, B_NAME);
    assertThatMapOf(normalRelationChildren(actualB)).containsExactlyInAnyOrder(tuple("MaybeC?", C_NAME));
  }

  @Test
  public void testNormalBiRelation() throws TransformationException {
    Root root = createRoot(createA(A_NAME), createC(C_NAME), createB(B_NAME));
    root.getC().setBiA1(root.getA());

    List<DumpNode> nodes = TestUtils.dumpModel(root);
    assertThat(nodes).flatExtracting(NAME_EXTRACTOR).containsExactlyInAnyOrder(ROOT_NAME, A_NAME, B_NAME, C_NAME);
    // bidirectional relations are currently not found, instead there will be two unidirectional ones
    DumpNode actualA = TestUtils.findByName(nodes, A_NAME);
    assertThatMapOf(normalRelationChildren(actualA)).containsExactlyInAnyOrder(tuple("BiC1", C_NAME));
    DumpNode actualC = TestUtils.findByName(nodes, C_NAME);
    assertThatMapOf(normalRelationChildren(actualC)).containsExactlyInAnyOrder(tuple("BiA1", A_NAME));
  }

  @Test
  public void testListBiRelation() throws TransformationException {
    Root root = createRoot(createA(A_NAME), createC(C_NAME), createB(B_NAME));
    root.getC().addBiA2(root.getA());

    List<DumpNode> nodes = TestUtils.dumpModel(root);
    assertThat(nodes).flatExtracting(NAME_EXTRACTOR).containsExactlyInAnyOrder(ROOT_NAME, A_NAME, B_NAME, C_NAME);
    // bidirectional relations are currently not found, instead there will be two unidirectional ones
    DumpNode actualA = TestUtils.findByName(nodes, A_NAME);
    assertThatMapOf(normalRelationChildren(actualA)).containsExactlyInAnyOrder(tuple("BiC2", C_NAME));
    DumpNode actualC = TestUtils.findByName(nodes, C_NAME);
    assertThatMapOf(listRelationChildren(actualC), "BiA2").containsExactlyInAnyOrder(A_NAME);
  }

  @Test
  public void testOptBiRelation() throws TransformationException {
    Root root = createRoot(createA(A_NAME), createC(C_NAME), createB(B_NAME));
    root.getC().setBiA3(root.getA());

    List<DumpNode> nodes = TestUtils.dumpModel(root);
    assertThat(nodes).flatExtracting(NAME_EXTRACTOR).containsExactlyInAnyOrder(ROOT_NAME, A_NAME, B_NAME, C_NAME);
    // bidirectional relations are currently not found, instead there will be two unidirectional ones
    DumpNode actualA = TestUtils.findByName(nodes, A_NAME);
    assertThatMapOf(normalRelationChildren(actualA)).containsExactlyInAnyOrder(tuple("BiC3", C_NAME));
    DumpNode actualC = TestUtils.findByName(nodes, C_NAME);
    assertThatMapOf(normalRelationChildren(actualC)).containsExactlyInAnyOrder(tuple("BiA3?", A_NAME));
  }

  @Test
  public void testOneNormalReferenceToken() throws TransformationException {
    Root root = createRoot(createA(A_NAME), createC(C_NAME), createB(B_NAME));
    root.getC().setRawReference(root.getA());

    List<DumpNode> nodes = TestUtils.dumpModel(root);
    assertThat(nodes).flatExtracting(NAME_EXTRACTOR).containsExactlyInAnyOrder(ROOT_NAME, A_NAME, B_NAME, C_NAME);
    DumpNode actualC = TestUtils.findByName(nodes, C_NAME);
    assertThatMapOf(referenceTokens(actualC)).containsExactlyInAnyOrder(tuple(TOKEN_LABEL_RAW_REFERENCE, A_NAME));
  }

  @Test
  public void testChildrenReorder1() throws TransformationException {
    C subC = new SubC();
    subC.setName(C_NAME);
    subC.setUnwanted(5);
    Root root = createRoot(createA(A_NAME), subC, createB(B_NAME));

    List<DumpNode> nodes = TestUtils.dumpModel(root);
    assertThat(nodes).flatExtracting(NAME_EXTRACTOR).containsExactlyInAnyOrder(ROOT_NAME, A_NAME, B_NAME, C_NAME);
    DumpNode actualRoot = TestUtils.findByName(nodes, ROOT_NAME);
    assertEquals(1, actualRoot.getNumDumpToken());
    assertEquals(3, actualRoot.getNumDumpChildNode());
    assertEquals(0, actualRoot.getNumDumpRelation());
    assertThat(valueTokens(findByName(nodes, C_NAME))).containsOnly(
        entry("Name", C_NAME),
        entry(TOKEN_LABEL_UNWANTED, 5));
  }

  @Test
  public void testLabel() throws TransformationException {
    Root root = createRoot(createA(A_NAME), null);

    List<DumpNode> nodes = TestUtils.dumpModel(root,
        builder -> builder.<ASTNode<?>>nodeStyle((node, style) -> style.setLabel(node.isA() ? "A" : "Not A")));
    DumpNode actualRoot = TestUtils.findByName(nodes, ROOT_NAME);
    assertEquals("Not A", actualRoot.getLabel());

    DumpNode actualA = TestUtils.findByName(nodes, A_NAME);
    assertEquals("A", actualA.getLabel());
  }

  @Test
  public void testTextColor() throws TransformationException {
    Root root = createRoot(createA(A_NAME), null);

    List<DumpNode> nodes = TestUtils.dumpModel(root,
        builder -> builder.<ASTNode<?>>nodeStyle((node, style) -> style.setTextColor(node.isA() ? "red" : "")));
    DumpNode actualRoot = TestUtils.findByName(nodes, ROOT_NAME);
    assertEquals("", actualRoot.getTextColor());

    DumpNode actualA = TestUtils.findByName(nodes, A_NAME);
    assertEquals("red", actualA.getTextColor());
  }

  @Test
  public void testBackgroundColor() throws TransformationException {
    Root root = createRoot(createA(A_NAME), null);

    List<DumpNode> nodes = TestUtils.dumpModel(root,
        builder -> builder.<ASTNode<?>>nodeStyle((node, style) -> style.setBackgroundColor(node.isA() ? "green" : "")));
    DumpNode actualRoot = TestUtils.findByName(nodes, ROOT_NAME);
    assertEquals("", actualRoot.getBackgroundColor());

    DumpNode actualA = TestUtils.findByName(nodes, A_NAME);
    assertEquals("green", actualA.getBackgroundColor());
  }

}
