package de.tudresden.inf.st.jastadd.testDumper;

import de.tudresden.inf.st.jastadd.dumpAst.ast.DumpNode;
import de.tudresden.inf.st.jastadd.dumpAst.ast.TransformationException;
import org.jastadd.testDumper.ast.Root;
import org.junit.jupiter.api.Test;

import java.util.List;

import static de.tudresden.inf.st.jastadd.testDumper.TestUtils.*;
import static org.assertj.core.api.Assertions.*;

public class TestIncluded {

  @Test
  public void testValueAttributeDefault() throws TransformationException {
    Root root = createRoot(null, null);

    List<DumpNode> nodes = TestUtils.dumpModel(root);
    assertThat(nodes).flatExtracting(NAME_EXTRACTOR).containsExactly(ROOT_NAME);
    DumpNode actualRoot = TestUtils.findByName(nodes, ROOT_NAME);
    assertThat(valueTokens(actualRoot)).containsOnly(entry("Name", ROOT_NAME));
  }

  @Test
  public void testValueAttributeIncluded() throws TransformationException {
    Root root = createRoot(null, null);

    List<DumpNode> nodes = TestUtils.dumpModel(root, db ->
        db.includeAttribute((node, attributeName, isNTA, value) -> !isNTA && attributeName.equals("simpleAttr")));
    assertThat(nodes).flatExtracting(NAME_EXTRACTOR).containsExactly(ROOT_NAME);
    DumpNode actualRoot = TestUtils.findByName(nodes, ROOT_NAME);
    assertThat(valueTokens(actualRoot)).containsOnly(entry("Name", ROOT_NAME), entry("simpleAttr", 42));
  }

  @Test
  public void testReferenceListAttributeIncluded() throws TransformationException {
    Root root = createRoot(null, null, createB(B1_NAME), createB(B2_NAME), createB(B3_NAME));

    List<DumpNode> nodes = TestUtils.dumpModel(root, db ->
        db.includeAttribute((node, attributeName, isNTA, value) -> !isNTA && attributeName.equals("setOfBs")));
    assertThat(nodes).flatExtracting(NAME_EXTRACTOR).containsExactly(ROOT_NAME, B1_NAME, B2_NAME, B3_NAME);

    DumpNode actualRoot = TestUtils.findByName(nodes, ROOT_NAME);
    assertThatMapOf(referenceListTokens(actualRoot), "setOfBs").containsOnly(
        B1_NAME, B2_NAME, B3_NAME);
  }

  @Test
  public void testReferenceAttributeDefault() throws TransformationException {
    Root root = createRoot(createA(A_NAME), null);

    List<DumpNode> nodes = TestUtils.dumpModel(root);
    assertThat(nodes).flatExtracting(NAME_EXTRACTOR).containsExactly(ROOT_NAME, A_NAME);
    DumpNode actualRoot = TestUtils.findByName(nodes, ROOT_NAME);
    assertThat(referenceTokens(actualRoot)).isEmpty();
  }

    @Test
  public void testReferenceAttributeIncluded() throws TransformationException {
    Root root = createRoot(createA(A_NAME), null);

    List<DumpNode> nodes = TestUtils.dumpModel(root, db ->
        db.includeAttribute((node, attributeName, isNTA, value) -> !isNTA && attributeName.equals("referenceAttr")));
    assertThat(nodes).flatExtracting(NAME_EXTRACTOR).containsExactly(ROOT_NAME, A_NAME);
    DumpNode actualRoot = TestUtils.findByName(nodes, ROOT_NAME);
    assertThatMapOf(referenceTokens(actualRoot)).containsOnly(tuple("referenceAttr", A_NAME));
  }

  @Test
  public void testNormalNTADefault() throws TransformationException {
    Root root = createRoot(null, createC(C_NAME));

    List<DumpNode> nodes = TestUtils.dumpModel(root);
    assertThat(nodes).flatExtracting(NAME_EXTRACTOR).containsExactly(ROOT_NAME, C_NAME);
    DumpNode actualC = TestUtils.findByName(nodes, C_NAME);
    assertThatMapOf(normalChildren(actualC)).isEmpty();
  }

  @Test
  public void testNormalNTAIncluded() throws TransformationException {
    Root root = createRoot(null, createC(C_NAME));

    List<DumpNode> nodes = TestUtils.dumpModel(root, db ->
        db.includeAttribute((node, attributeName, isNTA, value) -> isNTA && attributeName.equals("Calculated")));
    assertThat(nodes).flatExtracting(NAME_EXTRACTOR).containsExactly(ROOT_NAME, C_NAME, "Calculated-" + C_NAME);
    DumpNode actualC = TestUtils.findByName(nodes, C_NAME);
    assertThatMapOf(normalChildren(actualC)).containsOnly(tuple("Calculated", "Calculated-" + C_NAME));
  }

  @Test
  public void testListNTADefault() throws TransformationException {
    Root root = createRoot(null, createC(C_NAME));

    List<DumpNode> nodes = TestUtils.dumpModel(root);
    assertThat(nodes).flatExtracting(NAME_EXTRACTOR).containsExactly(ROOT_NAME, C_NAME);
    DumpNode actualC = TestUtils.findByName(nodes, C_NAME);
    assertThat(listChildren(actualC)).isEmpty();

  }

  @Test
  public void testListNTAIncluded() throws TransformationException {
    Root root = createRoot(null, createC(C_NAME));

    List<DumpNode> nodes = TestUtils.dumpModel(root, db ->
        db.includeAttribute((node, attributeName, isNTA, value) -> isNTA && attributeName.equals("AlsoCalculated")));
    assertThat(nodes).flatExtracting(NAME_EXTRACTOR).containsExactly(ROOT_NAME, C_NAME, "AlsoCalculated-" + C_NAME);
    DumpNode actualC = TestUtils.findByName(nodes, C_NAME);
    assertThatMapOf(listChildren(actualC), "AlsoCalculated").containsExactly("AlsoCalculated-" + C_NAME);
  }
}
