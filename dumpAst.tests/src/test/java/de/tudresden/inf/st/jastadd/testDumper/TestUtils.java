package de.tudresden.inf.st.jastadd.testDumper;

import de.tudresden.inf.st.jastadd.dumpAst.ast.*;
import org.assertj.core.api.AbstractListAssert;
import org.assertj.core.api.Assertions;
import org.assertj.core.api.ObjectAssert;
import org.assertj.core.groups.Tuple;
import org.jastadd.testDumper.ast.*;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.*;

public class TestUtils {
  public static final Random rand = new Random();

  public static final String TOKEN_LABEL_OTHER_VALUE = "OtherValue";
  public static final String TOKEN_LABEL_UNWANTED = "Unwanted";
  public static final String TOKEN_LABEL_RAW_REFERENCE = "RawReference";

  public static final String ROOT_NAME = "Root" + Integer.toHexString(rand.nextInt(0xFFFFFF));
  public static final String A_NAME = "A" + Integer.toHexString(rand.nextInt(0xFFFFFF));
  public static final String B_NAME = "B" + Integer.toHexString(rand.nextInt(0xFFFFFF));
  public static final String B1_NAME = "B1" + Integer.toHexString(rand.nextInt(0xFFFFFF));
  public static final String B2_NAME = "B2" + Integer.toHexString(rand.nextInt(0xFFFFFF));
  public static final String B3_NAME = "B3" + Integer.toHexString(rand.nextInt(0xFFFFFF));
  public static final String C_NAME = "C" + Integer.toHexString(rand.nextInt(0xFFFFFF));
  public static final String T1_NAME = "T1" + Integer.toHexString(rand.nextInt(0xFFFFFF));
  public static final String T2_NAME = "T2" + Integer.toHexString(rand.nextInt(0xFFFFFF));
  public static final String T3_NAME = "T3" + Integer.toHexString(rand.nextInt(0xFFFFFF));

  public static Root createRoot(A a, C c, B... bs) {
    Root result = new Root();
    result.setName(ROOT_NAME);
    if (a != null) {
      result.setA(a);
    }
    if (c != null) {
      result.setC(c);
    }
    for (B b : bs) {
      result.addB(b);
    }
    return result;
  }

  public static A createA(String name) {
    return createA(name, a -> {});
  }

  public static A createA(String name, Consumer<A> additionalSettings) {
    A result = new A();
    result.setName(name);
    additionalSettings.accept(result);
    return result;
  }

  public static A createA(String name, C myC) {
    return createA(name, a -> a.setMyC(myC));
  }

  public static B createB(String name) {
    return createB(name, b -> {});
  }

  public static B createB(String name, String otherValue) {
    return createB(name, b -> b.setOtherValue(otherValue));
  }

  public static B createB(String name, Consumer<B> additionalSettings) {
    B result = new B();
    result.setName(name);
    additionalSettings.accept(result);
    return result;
  }

  public static C createC(String name) {
    return createC(name, c -> {});
  }

  public static C createC(String name, A optA) {
    return createC(name, c -> c.setA(optA));
  }

  public static C createC(String name, Consumer<C> additionalSettings) {
    C result = new C();
    result.setName(name);
    additionalSettings.accept(result);
    return result;
  }

  public static final Function<DumpNode, String> NAME_EXTRACTOR = node -> {
    for (DumpToken dumpToken : node.getDumpTokenList()) {
      if (dumpToken.getLabel().equals("Name")) {
        return dumpToken.asDumpValueToken().getValue().toString();
      }
    }
    return null;
  };

  public static AbstractListAssert<?, List<? extends Tuple>, Tuple, ObjectAssert<Tuple>> assertThatMapOf(Map<String, DumpNode> map) {
    return Assertions.assertThat(map).extractingFromEntries(Map.Entry::getKey, e -> NAME_EXTRACTOR.apply(e.getValue()));
  }

  public static AbstractListAssert<?, List<?>, Object, ObjectAssert<Object>> assertThatMapOf(Map<String, List<DumpNode>> map, String key) {
    return Assertions.assertThat(map.get(key)).flatExtracting(NAME_EXTRACTOR);
  }

  public static void assertEmpty(DumpChildNode node) {
    assertEquals(1, node.asDumpListChildNode().getNumInnerDumpNode());
    assertTrue(node.asDumpListChildNode().getInnerDumpNode(0).getDumpNode().isEmpty());
  }

  public static void assertEmpty(DumpRelation node) {
    assertEquals(1, node.asDumpListRelation().getNumInnerRelationDumpNode());
    assertTrue(node.asDumpListRelation().getInnerRelationDumpNode(0).getDumpNode().isEmpty());
  }

  public static List<DumpNode> dumpModel(Object target) throws TransformationException {
    return dumpModel(target, db -> {});
  }

  public static List<DumpNode> dumpModel(Object target, Consumer<DumpBuilder> options) throws TransformationException {
    ExposingDumpBuilder builder = new ExposingDumpBuilder(target);
    builder.excludeNullNodes();
    options.accept(builder);
    DumpAst dumpAst = builder.build();
    // let mustache run, but ignore result
    dumpAst.toPlantUml();
    List<DumpNode> result = new ArrayList<>();
    for (DumpNode dumpNode : dumpAst.getDumpNodeList()) {
      if ((dumpNode.isAstNode() || dumpNode.isNull()) && !dumpNode.getInvisible()) {
        result.add(dumpNode);
      }
    }
    return result;
  }

  public static DumpNode findByName(List<DumpNode> nodes, String name) {
    for (DumpNode node : nodes) {
      if (name.equals(NAME_EXTRACTOR.apply(node))) {
        return node;
      }
    }
    Assertions.fail("No node found with name " + name);
    // DummyCode to keep intellij silent
    return new DumpNode();
  }

  public static Map<String, DumpNode> normalChildren(DumpNode node) {
    Map<String, DumpNode> result = new HashMap<>();
    for (DumpChildNode dumpChildNode : node.getDumpChildNodeList()) {
      if (!dumpChildNode.isList()) {
        // then it is a DumpNormalChildNode
        DumpNode target = ((DumpNormalChildNode) dumpChildNode).getDumpNode();
        if (target != null && !target.getInvisible()) {
          result.put(dumpChildNode.getLabel(), target);
        }
      }
    }
    return result;
  }

  public static Map<String, List<DumpNode>> listChildren(DumpNode node) {
    Map<String, List<DumpNode>> result = new HashMap<>();
    for (DumpChildNode dumpChildNode : node.getDumpChildNodeList()) {
      if (dumpChildNode.isList()) {
        // then it is a DumpListChildNode
        ((DumpListChildNode) dumpChildNode).getInnerDumpNodeList().forEach(inner -> {
          if (inner.getDumpNode() != null && !inner.getDumpNode().getInvisible()) {
            result.computeIfAbsent(dumpChildNode.getLabel(), key -> new ArrayList<>()).add(inner.getDumpNode());
          }
        });
      }
    }
    return result;
  }

  public static Map<String, DumpNode> normalRelationChildren(DumpNode node) {
    Map<String, DumpNode> result = new HashMap<>();
    for (DumpRelation dumpRelation : node.getDumpRelationList()) {
      if (!dumpRelation.isList()) {
        // then it is a DumpNormalRelation
        DumpNode target = ((DumpNormalRelation) dumpRelation).getDumpNode();
        if (!target.getInvisible()) {
          result.put(dumpRelation.getLabel(), target);
        }
      }
    }
    return result;
  }

  public static Map<String, List<DumpNode>> listRelationChildren(DumpNode node) {
    Map<String, List<DumpNode>> result = new HashMap<>();
    for (DumpRelation dumpRelation : node.getDumpRelationList()) {
      if (dumpRelation.isList()) {
        // then it is a DumpListRelation
        ((DumpListRelation) dumpRelation).getInnerRelationDumpNodeList().forEach(inner -> {
          if (!inner.getDumpNode().getInvisible()) {
            result.computeIfAbsent(dumpRelation.getLabel(), key -> new ArrayList<>()).add(inner.getDumpNode());
          }
        });
      }
    }
    return result;
  }

  public static Map<String, DumpNode> referenceTokens(DumpNode node) {
    Map<String, DumpNode> result = new HashMap<>();
    for (DumpToken dumpToken : node.getDumpTokenList()) {
      if (!dumpToken.isDumpValueToken() && !dumpToken.isList()) {
        // then it is a DumpReferenceToken
        DumpNode target = ((DumpReferenceToken) dumpToken).getDumpNode();
        if (!target.getInvisible()) {
          result.put(dumpToken.getLabel(), target);
        }
      }
    }
    return result;
  }

  public static Map<String, List<DumpNode>> referenceListTokens(DumpNode node) {
    Map<String, List<DumpNode>> result = new HashMap<>();
    for (DumpToken dumpToken : node.getDumpTokenList()) {
      if (!dumpToken.isDumpValueToken() && dumpToken.isList()) {
        // then it is a DumpReferenceListToken
        ((DumpReferenceListToken) dumpToken).getInnerRelationDumpNodeList().forEach(inner -> {
          if (!inner.getDumpNode().getInvisible()) {
            result.computeIfAbsent(dumpToken.getLabel(), key -> new ArrayList<>()).add(inner.getDumpNode());
          }
        });
      }
    }
    return result;
  }

  public static Map<String, Object> valueTokens(DumpNode node) {
    Map<String, Object> result = new HashMap<>();
    for (DumpToken dumpToken : node.getDumpTokenList()) {
      if (dumpToken.isDumpValueToken()) {
        // then it is a DumpValueToken
        DumpValueToken dumpValueToken = (DumpValueToken) dumpToken;
        result.put(dumpValueToken.getLabel(), dumpValueToken.getValue());
      }
    }
    return result;
  }

  public static List<DumpNode> invisiblePath(DumpNode node) {
    List<DumpNode> result = new ArrayList<>();
    for (InnerRelationDumpNode inner : node.getInvisiblePath().getInnerRelationDumpNodeList()) {
      DumpNode target = inner.getDumpNode();
      assertFalse(target.getInvisible());
      result.add(target);
    }
    return result;
  }

  static class ExposingDumpBuilder extends DumpBuilder {

    protected ExposingDumpBuilder(Object target) {
      super(target);
    }

    @Override
    public DumpAst build() throws TransformationException {
      return super.build();
    }
  }

}
