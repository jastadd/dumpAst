package de.tudresden.inf.st.jastadd.testDumper;

import de.tudresden.inf.st.jastadd.dumpAst.ast.DumpBuilder;
import de.tudresden.inf.st.jastadd.dumpAst.ast.DumpNode;
import de.tudresden.inf.st.jastadd.dumpAst.ast.Dumper;
import de.tudresden.inf.st.jastadd.dumpAst.ast.TransformationException;
import org.jastadd.testDumper.ast.A;
import org.jastadd.testDumper.ast.Root;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;

import static de.tudresden.inf.st.jastadd.testDumper.TestUtils.*;
import static org.assertj.core.api.Assertions.*;

public class TestExcluded {

  @Test
  public void testEmptyStringsOnTokensDefault() throws TransformationException {
    Root root = createRoot(null, null, createB(B1_NAME, "something"), createB(B2_NAME), createB(B3_NAME));

    // normal mode, do not include empty strings
    List<DumpNode> nodes = TestUtils.dumpModel(root);
    assertThat(nodes).flatExtracting(NAME_EXTRACTOR).containsExactlyInAnyOrder(ROOT_NAME, B1_NAME, B2_NAME, B3_NAME);
    DumpNode actualB1 = TestUtils.findByName(nodes, B1_NAME);
    assertThat(valueTokens(actualB1)).containsOnly(entry("Name", B1_NAME), entry(TOKEN_LABEL_OTHER_VALUE, "something"));
    DumpNode actualB2 = TestUtils.findByName(nodes, B2_NAME);
    assertThat(valueTokens(actualB2)).containsOnly(entry("Name", B2_NAME));
    DumpNode actualB3 = TestUtils.findByName(nodes, B3_NAME);
    assertThat(valueTokens(actualB3)).containsOnly(entry("Name", B3_NAME));
  }

  @Test
  public void testEmptyStringsOnTokensIncluded() throws TransformationException {
    Root root = createRoot(null, null, createB(B1_NAME, "something"), createB(B2_NAME), createB(B3_NAME));

    // test mode, do include empty strings
    List<DumpNode> nodes = TestUtils.dumpModel(root, DumpBuilder::includeEmptyStringsOnTokens);
    assertThat(nodes).flatExtracting(NAME_EXTRACTOR).containsExactlyInAnyOrder(ROOT_NAME, B1_NAME, B2_NAME, B3_NAME);
    DumpNode actualB1 = TestUtils.findByName(nodes, B1_NAME);
    assertThat(valueTokens(actualB1)).containsOnly(entry("Name", B1_NAME), entry(TOKEN_LABEL_OTHER_VALUE, "something"));
    DumpNode actualB2 = TestUtils.findByName(nodes, B2_NAME);
    assertThat(valueTokens(actualB2)).containsOnly(entry("Name", B2_NAME), entry(TOKEN_LABEL_OTHER_VALUE, ""));
    DumpNode actualB3 = TestUtils.findByName(nodes, B3_NAME);
    assertThat(valueTokens(actualB3)).containsOnly(entry("Name", B3_NAME), entry(TOKEN_LABEL_OTHER_VALUE, ""));
  }

  private Root setupAllRelations() {
    Root root = createRoot(createA(A_NAME), createC(C_NAME), createB(B_NAME, "A"));
    root.getC().setRawReference(root.getA());
    // rel B.oneA -> A ;
    root.getB(0).setOneA(root.getA());
    // rel B.maybeC? -> C ;
    root.getB(0).addManyA(root.getA());
    // rel B.manyA* -> A ;
    root.getB(0).setMaybeC(root.getC());
    // rel C.biA1 <-> A.biC1 ;
    root.getC().setBiA1(root.getA());
    // rel C.biA2* <-> A.biC2 ;
    root.getC().addBiA2(root.getA());
    // rel C.biA3? <-> A.biC3 ;
    root.getC().setBiA3(root.getA());
    return root;
  }

  @Test
  public void testTypesDefault() throws TransformationException {
    Root root = setupAllRelations();

    List<DumpNode> nodes = TestUtils.dumpModel(root);
    assertThat(nodes).flatExtracting(NAME_EXTRACTOR).containsExactlyInAnyOrder(ROOT_NAME, A_NAME, B_NAME, C_NAME);
    assertAllRelationsDefaultMatches(nodes);
  }

  private void assertAllRelationsDefaultMatches(List<DumpNode> nodes) {
    // A
    assertThatMapOf(normalRelationChildren(findByName(nodes, A_NAME))).containsExactlyInAnyOrder(
        tuple("BiC1", C_NAME), tuple("BiC2", C_NAME), tuple("BiC3", C_NAME));
    // B
    assertThatMapOf(normalRelationChildren(findByName(nodes, B_NAME))).containsExactlyInAnyOrder(
        tuple("OneA", A_NAME), tuple("MaybeC?", C_NAME));
    // C
    assertThatMapOf(normalRelationChildren(findByName(nodes, C_NAME))).containsExactlyInAnyOrder(
        tuple("BiA1", A_NAME), tuple("BiA3?", A_NAME));
    assertThatMapOf(listRelationChildren(findByName(nodes, C_NAME)), "BiA2").containsExactlyInAnyOrder(A_NAME);
    assertThatMapOf(referenceTokens(findByName(nodes, C_NAME))).containsExactlyInAnyOrder(
        tuple(TOKEN_LABEL_RAW_REFERENCE, A_NAME));
  }

  @Test
  public void testTypesNonMatching() throws TransformationException {
    Root root = setupAllRelations();

    List<DumpNode> nodes = TestUtils.dumpModel(root, db -> db.disableTypes("NonExistingType"));
    assertThat(nodes).flatExtracting(NAME_EXTRACTOR).containsExactlyInAnyOrder(ROOT_NAME, A_NAME, B_NAME, C_NAME);
    assertAllRelationsDefaultMatches(nodes);
  }

  @Test
  public void testTypesInheritanceNotIncluded() throws TransformationException {
    Root root = setupAllRelations();

    List<DumpNode> nodes = TestUtils.dumpModel(root, db -> db.disableTypes("Nameable"));
    assertThat(nodes).flatExtracting(NAME_EXTRACTOR).containsExactlyInAnyOrder(ROOT_NAME, A_NAME, B_NAME, C_NAME);
    assertAllRelationsDefaultMatches(nodes);
  }

  @Test
  public void testTypesExcludeOnlyA() throws TransformationException {
    Root root = setupAllRelations();

    List<DumpNode> nodes = TestUtils.dumpModel(root, db -> db.disableTypes("A"));
    assertThat(nodes).flatExtracting(NAME_EXTRACTOR).containsExactlyInAnyOrder(ROOT_NAME, B_NAME, C_NAME);
    // B
    assertThatMapOf(normalRelationChildren(findByName(nodes, B_NAME))).containsExactlyInAnyOrder(
        tuple("MaybeC?", (C_NAME)));
    // C
    assertThat(normalRelationChildren(findByName(nodes, C_NAME))).isEmpty();
    assertThat(listRelationChildren(findByName(nodes, C_NAME))).isEmpty();
    assertThat(referenceTokens(findByName(nodes, C_NAME))).isEmpty();
  }

  @Test
  public void testTypesExcludeMultipleArguments() throws TransformationException {
    Root root = setupAllRelations();

    List<DumpNode> nodes = TestUtils.dumpModel(root, db -> db.disableTypes("A", "C"));
    assertThat(nodes).flatExtracting(NAME_EXTRACTOR).containsExactlyInAnyOrder(ROOT_NAME, B_NAME);
    // B
    assertThat(normalRelationChildren(findByName(nodes, B_NAME))).isEmpty();
  }

  @Test
  public void testTypesExcludeMultipleCalls() throws TransformationException {
    Root root = setupAllRelations();

    List<DumpNode> nodes = TestUtils.dumpModel(root, db -> db.disableTypes("A").disableTypes("C"));
    assertThat(nodes).flatExtracting(NAME_EXTRACTOR).containsExactlyInAnyOrder(ROOT_NAME, B_NAME);
    // B
    assertThat(normalRelationChildren(findByName(nodes, B_NAME))).isEmpty();
  }

  @Test
  public void testTypesExcludeRegex() throws TransformationException {
    Root root = setupAllRelations();

    List<DumpNode> nodes = TestUtils.dumpModel(root, db -> db.disableTypes("Ro*t"));
    assertThat(nodes).flatExtracting(NAME_EXTRACTOR).containsExactlyInAnyOrder(A_NAME, B_NAME, C_NAME);
    assertAllRelationsDefaultMatches(nodes);
  }

  @Test
  public void testTypesInvisiblePath() throws TransformationException {
    final String A2_Name = "a2";
    final String A3_Name = "a3";
    final String C2_Name = "c2";
    Root root = createRoot(createA(A_NAME,
        createC(C_NAME,
            createA(A2_Name,
                createC(C2_Name,
                    createA(A3_Name, a -> a.setB(createB(B_NAME))))))),
        null);

    List<DumpNode> nodes = TestUtils.dumpModel(root, db -> db.disableTypes("A", "C"));
    DumpNode actualRoot = findByName(nodes, ROOT_NAME);
    assertThat(invisiblePath(actualRoot)).flatExtracting(NAME_EXTRACTOR).containsExactly(B_NAME);
  }

  @Test
  public void testTokenDefault() throws TransformationException {
    A a = createA(A_NAME);
    Root root = createRoot(a, createC(C_NAME, c -> {
      c.setRawReference(a);
      c.setUnwanted(5);
      c.setBiA1(a);
    }));

    List<DumpNode> nodes = TestUtils.dumpModel(root);
    assertThat(nodes).flatExtracting(NAME_EXTRACTOR).containsExactlyInAnyOrder(ROOT_NAME, A_NAME, C_NAME);
    DumpNode actualC = findByName(nodes, C_NAME);
    assertThat(valueTokens(actualC)).containsOnly(
        entry("Name", C_NAME),
        entry(TOKEN_LABEL_UNWANTED, 5));
    assertThatMapOf(referenceTokens(actualC)).containsOnly(tuple(TOKEN_LABEL_RAW_REFERENCE, A_NAME));
    assertThatMapOf(normalRelationChildren(findByName(nodes, C_NAME))).containsExactlyInAnyOrder(
        tuple("BiA1", A_NAME));
  }

  @Test
  public void testTokenExcludeValue() throws TransformationException {
    A a = createA(A_NAME);
    Root root = createRoot(a, createC(C_NAME, c -> {
      c.setRawReference(a);
      c.setUnwanted(5);
      c.setBiA1(a);
    }));

    List<DumpNode> nodes = TestUtils.dumpModel(root, db -> db.includeToken((node, tokenName, value) ->
        !tokenName.equals(TOKEN_LABEL_UNWANTED)));
    assertThat(nodes).flatExtracting(NAME_EXTRACTOR).containsExactlyInAnyOrder(ROOT_NAME, A_NAME, C_NAME);
    DumpNode actualC = findByName(nodes, C_NAME);
    assertThat(valueTokens(actualC)).containsOnly(
        entry("Name", C_NAME));
    assertThatMapOf(referenceTokens(actualC)).containsOnly(tuple(TOKEN_LABEL_RAW_REFERENCE, A_NAME));
    assertThatMapOf(normalRelationChildren(findByName(nodes, C_NAME))).containsExactlyInAnyOrder(
        tuple("BiA1", A_NAME));

  }

  @Test
  public void testTokenExcludeValueAndReference() throws TransformationException {
    A a = createA(A_NAME);
    Root root = createRoot(a, createC(C_NAME, c -> {
      c.setRawReference(a);
      c.setUnwanted(5);
      c.setBiA1(a);
    }));

    List<DumpNode> nodes = TestUtils.dumpModel(root, db -> db.includeToken((node, tokenName, value) ->
        !tokenName.equals(TOKEN_LABEL_UNWANTED) && !tokenName.equals(TOKEN_LABEL_RAW_REFERENCE)));
    assertThat(nodes).flatExtracting(NAME_EXTRACTOR).containsExactlyInAnyOrder(ROOT_NAME, A_NAME, C_NAME);
    DumpNode actualC = findByName(nodes, C_NAME);
    assertThat(valueTokens(actualC)).containsOnly(
        entry("Name", C_NAME));
    assertThat(referenceTokens(actualC)).isEmpty();
    assertThatMapOf(normalRelationChildren(findByName(nodes, C_NAME))).containsExactlyInAnyOrder(
        tuple("BiA1", A_NAME));

  }

  @Test
  public void testTokenExcludeRelationNameLowerCase() throws TransformationException {
    A a = createA(A_NAME);
    Root root = createRoot(a, createC(C_NAME, c -> {
      c.setRawReference(a);
      c.setUnwanted(5);
      c.setBiA1(a);
    }));

    List<DumpNode> nodes = TestUtils.dumpModel(root, db -> db.includeToken((node, tokenName, value) ->
        !tokenName.startsWith("bi")));
    assertThat(nodes).flatExtracting(NAME_EXTRACTOR).containsExactlyInAnyOrder(ROOT_NAME, A_NAME, C_NAME);
    DumpNode actualC = findByName(nodes, C_NAME);
    assertThat(valueTokens(actualC)).containsOnly(
        entry("Name", C_NAME),
        entry(TOKEN_LABEL_UNWANTED, 5));
    assertThatMapOf(referenceTokens(actualC)).containsOnly(tuple(TOKEN_LABEL_RAW_REFERENCE, A_NAME));
    assertThatMapOf(normalRelationChildren(findByName(nodes, C_NAME))).containsExactlyInAnyOrder(
        tuple("BiA1", A_NAME));
  }

  @Test
  public void testTokenExcludeRelationNameTitleCase() throws TransformationException {
    A a = createA(A_NAME);
    Root root = createRoot(a, createC(C_NAME, c -> {
      c.setRawReference(a);
      c.setUnwanted(5);
      c.setBiA1(a);
    }));

    List<DumpNode> nodes = TestUtils.dumpModel(root, db -> db.includeToken((node, tokenName, value) ->
        !tokenName.startsWith("Bi")));
    assertThat(nodes).flatExtracting(NAME_EXTRACTOR).containsExactlyInAnyOrder(ROOT_NAME, A_NAME, C_NAME);
    DumpNode actualC = findByName(nodes, C_NAME);
    assertThat(valueTokens(actualC)).containsOnly(
        entry("Name", C_NAME),
        entry(TOKEN_LABEL_UNWANTED, 5));
    assertThatMapOf(referenceTokens(actualC)).containsOnly(tuple(TOKEN_LABEL_RAW_REFERENCE, A_NAME));
    assertThatMapOf(normalRelationChildren(findByName(nodes, C_NAME))).containsExactlyInAnyOrder(
        tuple("BiA1", A_NAME));

  }

  @Test
  public void testChildrenDefault() throws TransformationException {
    /*  Root
        |- a
           |- b  - - -.
           |-MyC: c   v
                  |- a2
                     |- b2
    */
    final String A2_Name = "a2";
    Root root = createRoot(createA(A_NAME, a -> {
      a.setB(createB(B_NAME));
      a.setMyC(createC(C_NAME,
          createA(A2_Name, a2 -> a2.setB(createB(B2_NAME)))));
    }), null);
    root.getA().getB().setOneA(root.getA().getMyC().getA());

    List<DumpNode> nodes = TestUtils.dumpModel(root);
    assertThat(nodes).flatExtracting(NAME_EXTRACTOR).containsExactlyInAnyOrder(
        ROOT_NAME, A_NAME, A2_Name, B_NAME, B2_NAME, C_NAME);
    DumpNode actualA = findByName(nodes, A_NAME);
    assertThatMapOf(normalChildren(actualA)).containsExactlyInAnyOrder(
        tuple("MyC", C_NAME), tuple("B", B_NAME));
  }

  @Test
  public void testChildrenExclude() throws TransformationException {
    /*  Root
        |- a
           |- b  - - -.
           |-MyC: c   v
                  |- a2
                     |- b2
    */
    final String A2_Name = "a2";
    Root root = createRoot(createA(A_NAME, a -> {
      a.setB(createB(B_NAME));
      a.setMyC(createC(C_NAME,
          createA(A2_Name, a2 -> a2.setB(createB(B2_NAME)))));
    }), null);
    root.getA().getB().setOneA(root.getA().getMyC().getA());

    List<DumpNode> nodes = TestUtils.dumpModel(root, db -> db.includeChild((parentNode, childNode, contextName) ->
        !contextName.equals("MyC")));
    assertThat(nodes).flatExtracting(NAME_EXTRACTOR).containsExactlyInAnyOrder(
        ROOT_NAME, A_NAME, B_NAME, A2_Name, B2_NAME);
    DumpNode actualA = findByName(nodes, A_NAME);
    assertThatMapOf(normalChildren(actualA)).containsExactlyInAnyOrder(tuple("B", B_NAME));
  }

  @Test
  public void testRelationsDefault() throws TransformationException {
    // actually the same test as #testTypesDefault, but can't hurt to test twice
    Root root = setupAllRelations();

    List<DumpNode> nodes = TestUtils.dumpModel(root);
    assertThat(nodes).flatExtracting(NAME_EXTRACTOR).containsExactlyInAnyOrder(ROOT_NAME, A_NAME, B_NAME, C_NAME);
    assertAllRelationsDefaultMatches(nodes);
  }

  @Test
  public void testRelationsExclude() throws TransformationException {
    Root root = setupAllRelations();

    List<DumpNode> nodes = TestUtils.dumpModel(root, db -> db.includeRelation((sourceNode, targetNode, roleName) ->
        !roleName.equals("BiC1")));
    // A
    assertThatMapOf(normalRelationChildren(findByName(nodes, A_NAME))).containsExactlyInAnyOrder(
        tuple("BiC2", C_NAME), tuple("BiC3", C_NAME));
    // B
    assertThatMapOf(normalRelationChildren(findByName(nodes, B_NAME))).containsExactlyInAnyOrder(
        tuple("OneA", A_NAME), tuple("MaybeC?", C_NAME));
    // C
    assertThatMapOf(normalRelationChildren(findByName(nodes, C_NAME))).containsExactlyInAnyOrder(
        tuple("BiA1", A_NAME), tuple("BiA3?", A_NAME));
    assertThatMapOf(listRelationChildren(findByName(nodes, C_NAME)), "BiA2").containsExactlyInAnyOrder(A_NAME);
    assertThatMapOf(referenceTokens(findByName(nodes, C_NAME))).containsExactlyInAnyOrder(
        tuple(TOKEN_LABEL_RAW_REFERENCE, A_NAME));
  }

  @Test
  public void testRelationsExcludeRegex1() throws TransformationException {
    Root root = setupAllRelations();

    List<DumpNode> nodes = TestUtils.dumpModel(root, db -> db.includeRelation((sourceNode, targetNode, roleName) ->
        !roleName.startsWith("Bi")));
    // A
    assertThat(normalRelationChildren(findByName(nodes, A_NAME))).isEmpty();
    // B
    assertThatMapOf(normalRelationChildren(findByName(nodes, B_NAME))).containsExactlyInAnyOrder(
        tuple("OneA", A_NAME), tuple("MaybeC?", C_NAME));
    // C
    assertThat(normalRelationChildren(findByName(nodes, C_NAME))).isEmpty();
    assertEmpty(TestUtils.findByName(nodes, C_NAME).getDumpRelation(0));
    assertThatMapOf(referenceTokens(findByName(nodes, C_NAME))).containsExactlyInAnyOrder(
        tuple(TOKEN_LABEL_RAW_REFERENCE, A_NAME));
  }

  @Test
  public void testRelationsExcludeRegex2() throws TransformationException {
    Root root = setupAllRelations();

    List<DumpNode> nodes = TestUtils.dumpModel(root, db -> db.includeRelation((sourceNode, targetNode, roleName) ->
        !roleName.contains("A")));
    // A
    assertThatMapOf(normalRelationChildren(findByName(nodes, A_NAME))).containsExactlyInAnyOrder(
        tuple("BiC1", C_NAME), tuple("BiC2", C_NAME), tuple("BiC3", C_NAME));
    // B
    assertThatMapOf(normalRelationChildren(findByName(nodes, B_NAME))).containsExactlyInAnyOrder(
        tuple("MaybeC?", C_NAME));
    // C
    assertThat(normalRelationChildren(findByName(nodes, C_NAME))).isEmpty();
    assertEmpty(TestUtils.findByName(nodes, C_NAME).getDumpRelation(0));
    assertThatMapOf(referenceTokens(findByName(nodes, C_NAME))).containsExactlyInAnyOrder(
        tuple(TOKEN_LABEL_RAW_REFERENCE, A_NAME));
  }

  // --- copy from below here ---

  @Test
  public void testXDefault() {

  }

  @Test
  public void testXExclude() {

  }

}
