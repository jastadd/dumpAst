// --- Buildscripts (must be at the top) ---
buildscript {
    repositories.mavenCentral()
    dependencies {
        classpath group: 'org.jastadd', name: 'jastaddgradle', version: '1.13.3'
    }
}

// --- Plugin definitions ---
plugins {
    id 'java'
    id 'java-library'
    id 'idea'
    id 'com.github.ben-manes.versions'
}

apply plugin: 'jastadd'

// --- Dependencies ---
repositories {
    mavenCentral()
    maven {
        name 'gitlab-maven'
        url 'https://git-st.inf.tu-dresden.de/api/v4/groups/jastadd/-/packages/maven'
    }
}

configurations {
    relast
}

dependencies {
    implementation project(':dumpAst.base')
    relast group: 'org.jastadd', name: 'relast', version: "${relast_version}"
    jastadd2 "org.jastadd:jastadd2:2.3.5-dresden-6"

    implementation group: 'net.sf.beaver', name: 'beaver-rt', version: '0.9.11'

    testImplementation group: 'org.junit.jupiter', name: 'junit-jupiter-api', version: "${jupiter_version}"
    testImplementation group: 'org.assertj', name: 'assertj-core', version: '3.18.1'
    testRuntimeOnly group: 'org.junit.jupiter', name: 'junit-jupiter-engine', version: "${jupiter_version}"
}


// --- Preprocessors ---
File genSrc = file("src/gen/java")
sourceSets.main.java.srcDir genSrc
idea.module.generatedSourceDirs += genSrc

File testingGrammar = file('./src/main/jastadd/testDumper.relast')

task relast(type: JavaExec) {
    group = 'Build'
    classpath = configurations.relast
    mainClass = 'org.jastadd.relast.compiler.Compiler'

    doFirst {
        delete "src/gen/jastadd/*"
        mkdir  "src/gen/jastadd/"
    }

    args = [
            testingGrammar,
            "--listClass=java.util.ArrayList",
            "--jastAddList=JastAddList",
            "--useJastAddNames",
            "--file",
            "--resolverHelper",
            "--grammarName=./src/gen/jastadd/testDumper"
    ]
}

// --- JastAdd ---
jastadd {
    configureModuleBuild()
    modules {
        //noinspection GroovyAssignabilityCheck
        module("testDumper") {
            jastadd {
                basedir "."
                include "src/main/jastadd/**/*.ast"
                include "src/main/jastadd/**/*.jadd"
                include "src/main/jastadd/**/*.jrag"
                include "src/gen/jastadd/**/*.ast"
                include "src/gen/jastadd/**/*.jadd"
                include "src/gen/jastadd/**/*.jrag"
            }
        }
    }

    cleanGen.doFirst {
        delete "src/gen/java/org"
        delete "src/gen-res/BuildInfo.properties"
    }

    preprocessParser.doFirst {

        args += ["--no-beaver-symbol"]

    }

    module = "testDumper"
    astPackage = 'org.jastadd.testDumper.ast'
    genDir = 'src/gen/java'
    buildInfoDir = 'src/gen-res'
    jastaddOptions = ["--lineColumnNumbers", "--List=JastAddList", "--safeLazy", "--visitCheck=true", "--rewrite=cnta", "--cache=all", "--incremental=param", "--tracing=cache,flush"]
}

// --- Tests ---
test.useJUnitPlatform()

// --- Versioning and Publishing ---

// --- Task order ---
generateAst.dependsOn relast
