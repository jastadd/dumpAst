# Using DumpAst

After [adding DumpAst to your project](adding.md), you can start getting snapshot of some (part of an) AST.
Here is one example with a `Model` containing nodes of type `Robot`, `Joint` and `EndEffector` amongst others.
It is explained in detail below.

```java
import de.tudresden.inf.st.jastadd.dumpAst.ast.*;

import your.model.Robot;
import your.model.Model;
import your.model.ASTNode;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Main {
  public static void main(String[] args) throws IOException, TransformationException {
    Model model = createModel();  // (1)

    DumpBuilder builder = Dumper.read(model)  // (2)
        .includeChild((parentNode, childNode, contextName) -> {  // (3)
          if (parentNode instanceof Robot && ((Robot) parentNode).getName().equals("Robot2")) {
            return false;
          }
          return !contextName.equals("EndEffector");
        })
        .nodeStyle((node, style) -> {
          style.useSimpleLabel();  // (4)
          style.setBackgroundColor(node.size() > 30 ? "blue" : "white");  // (5)
        })
        .relationStyle((source, target, isComputed, isContainment, style) -> {
          if (style.getLabel().equals("EndEffector")) {
            style.setLabel("");  // (6)
          }
        })
        .skinParam(SkinParamBooleanSetting.Shadowing, false);  // (7)
    builder.dumpAsPNG(Paths.get("featureTest.png"));  // (8)
    builder.dumpAsSVG(Paths.get("featureTest.svg"));  // (9)
    OutputStream os = getMyOutputStream();
    builder.dumpAsPNG(os);  // (10)
  }
}
```

DumpAst uses the [Builder design pattern](https://en.wikipedia.org/wiki/Builder_pattern) as well as a [fluent API](https://en.wikipedia.org/wiki/Fluent_interface).
That means, first, all settings are specified (2-6), and only after a "dump"-method is called (7,8) the actual snapshot is taken.
For filtering the output, there are various lambda functions (`include___`).
Those methods are available for children, attributes, relations and tokens.
By default, all children, relations and tokens are included in the output, whereas all attributes are excluded.

The steps in detail:

- (1) In the beginning, your model is somehow constructed and changed. This is indicated with the method `createModel`.
- (2) The first call is always `Dumper.read()` to which the (part of the) AST is provided as an argument. It returns an object of type [DumpBuilder](../ragdoc/#/type/DumpBuilder). With this object, the output can be changed.
- (3) [Optional] You can only include nodes of certain types by using `includeChild` and passing a lambda function, which is called for every node and should return `true` if the `childNode` shall be included in the output. It defaults to returning `true` for every node. Here, all children of a `Robot` node (except if the name of the robot is `"Robot2"`) and all children of the context named `"EndEffector"` are excluded.
- (4) and (5) [Optional] To style a node, the method `nodeStyle` is used. It accepts a lambda function, in which the style of every node can be customized by calling methods on the given `Style` object, e.g. setting the name (5) or background color (6). The default name is to use the class name along with the hashcode of the object (`node == null ? "null" : node.getClass().getSimpleName() + "@" + Integer.toHexString(node.hashCode())`).
- (6) [Optional] To style a relation, the method `relationStyle` is used. It is used similar to `nodeStyle`, but is called with source and target node of a relation (containment and non-containment). Note, that (a) the default label can be retrieved with `style.getLabel()`, (b) `target` can be `null` for lists, (c) secondary connections from a list node to the target nodes use the style of the relation and use their index as label.
- (7) [Optional] There are a few ways to style the output in general through [SkinParam](https://plantuml.com/de/skinparam). Not all of those parameters are supported; see `SkinParamBooleanSetting` and `SkinParamStringSetting`.
- (8), (9) and (10) To create an output image, use `dumpAsPNG` for PNG (`dumpAsSVG` for SVG). Those methods do not return the builder object, as they produce an output. They potentially throw two kinds of exception: `IOException` (when the file could not be written) and `TransformationException` (when the AST could not be processed). As shown here, the builder object must be stored in a variable to create multiple outputs. Alternatively to a file, the result can be written to a given `OutputStream` (10).

To summarise the required steps: Specify the AST to read in, configure the output, and write ("dump") the output.
For more configuration options, please consult the [API Docs of DumpBuilder](../ragdoc/#/type/DumpBuilder) or check out [FeatureTestMain](https://git-st.inf.tu-dresden.de/jastadd/dumpAst/-/blob/main/dumpAst.prototyping/src/main/java/de/tudresden/inf/st/jastadd/featureTest/FeatureTestMain.java).
