# DumpAst

The tool called `DumpAst` ([see in repo](https://git-st.inf.tu-dresden.de/jastadd/dumpAst)) is used to create a snapshot of an AST and visualise it.

![](img/dumpAst.png)

It has to be used within your application code, and comes with a fluent interface to add and/or filter parts of the AST.
First, import the entry point class `Dumper`

```java
import de.tudresden.inf.st.jastadd.dumpAst.ast.Dumper;
```

Then, read in the ASTNode in question:

```java
Dumper.read(astNode)
```

Using the return value (a `DumpBuilder`), use methods to filter out unwanted parts and add styling or other settings.
All methods can be chained together.
Please see [the subpage on using DumpAst](using.md) and the [API documentation](ragdoc/index.html) for more details.
