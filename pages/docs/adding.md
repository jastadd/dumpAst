# Add DumpAst to your project

Check the [package overview page](https://git-st.inf.tu-dresden.de/jastadd/dumpAst/-/packages) to find the latest versions of the individual packages.

To use `dumpAst`, adjust your `build.gradle` as follows.

Set up the maven package source as a repository:

```
repositories {
    maven {
        name "gitlab-maven"
        url "https://git-st.inf.tu-dresden.de/api/v4/groups/jastadd/-/packages/maven"
    }
}
```

Add `dumpAst` as a dependency:

```
dependencies {
    implementation group: 'de.tudresden.inf.st', name: 'dumpAst', version: '{{dumpAst_version()}}'
}
```

## Build from source (not recommended)

If you want to build the tools of `DumpAst` from source, first build the jar from the [repository](https://git-st.inf.tu-dresden.de/jastadd/dumpAst).
The normal jar does not suffice, as it lacks the information on needed dependencies.

```bash
git clone https://git-st.inf.tu-dresden.de/jastadd/dumpAst.git
cd dumpAst
./gradlew fatJar
ls dumpAst.base/build/libs/
```

This jar can then be copied to your project.

```bash
cp dumpAst.base/build/libs/dumpAst.base-fatJar-{{dumpAst_version()}}.jar ../your-project/libs/dumpAst.jar
cd ../your-project/
```

Finally, this jar has to be integrated into your build process.

```
dependencies {
    implementation fileTree(include: ['dumpAst.jar'], dir: 'libs')
}
```

The path to the jar file may need to be changed according to your project structure.
