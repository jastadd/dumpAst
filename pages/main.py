import os

dumpAstVersionFileName = '../dumpAst.base/src/main/resources/dumpAstVersion.properties'


def get_version():
    if os.environ.get('CI_COMMIT_BRANCH', 'unknown') == 'dev':
        return 'dev'
    with open(dumpAstVersionFileName) as dumpAstVersionFile:
        versionFileContent = dumpAstVersionFile.read()
    return versionFileContent[versionFileContent.rindex('version=') + 8:].strip()


def define_env(env):
    """
    This is the hook for defining variables, macros and filters

    - variables: the dictionary that contains the environment variables
    - macro: a decorator function, to declare a macro.
    """
    env.conf['site_name'] = 'dumpAst ' + get_version()

    @env.macro
    def dumpAst_version():
        return get_version()


if __name__ == '__main__':
    print(get_version())
