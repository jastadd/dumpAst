package de.tudresden.inf.st.jastadd.dumpAst.ast;

public enum SkinParamBooleanSetting {
  /**
   * Print in grayscale?
   */
  Monochrome,
  /**
   * Use shadows?
   */
  Shadowing,
  /**
   * Use handwritten style?
   */
  Handwritten
}
