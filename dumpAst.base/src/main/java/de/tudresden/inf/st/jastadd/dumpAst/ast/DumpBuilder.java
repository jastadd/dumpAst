package de.tudresden.inf.st.jastadd.dumpAst.ast;

import net.sourceforge.plantuml.FileFormat;
import net.sourceforge.plantuml.FileFormatOption;
import net.sourceforge.plantuml.SourceStringReader;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ResourceBundle;
import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * Creating a snapshot of an AST.
 */
@SuppressWarnings("UnusedReturnValue")
public class DumpBuilder {
  private final Object target;
  private String packageName;
  private DumpAst dumpAst;

  private boolean built;

  protected DumpBuilder(Object target) {
    this.target = target;
    this.built = false;
    this.dumpAst = new DumpAst();
    dumpAst.setBuildConfig(new BuildConfig());
    dumpAst.getBuildConfig().setIncludeChildMethod((parentNode, childNode, contextName) -> true);
    dumpAst.getBuildConfig().setIncludeRelationMethod((sourceNode, targetNode, roleName) -> true);
    dumpAst.getBuildConfig().setIncludeTokenMethod((node, tokenName, value) -> true);
    dumpAst.getBuildConfig().setIncludeAttributeMethod((node, attributeName, isNTA, supplier) -> false);
    dumpAst.getBuildConfig().setStyleInformation(StyleInformation.createDefault());
    dumpAst.setPrintConfig(new PrintConfig());
    dumpAst.getPrintConfig().setScale(1);
    dumpAst.getPrintConfig().setVersion(readVersion());
    dumpAst.getPrintConfig().setNodeStyleDefinition((node, style) -> {});
    dumpAst.getPrintConfig().setRelationStyleDefinition((sourceNode, targetNode, isComputed, isContainment, style) -> {});
  }

  private DumpBuilder thisWithResetBuilt() {
    this.built = false;
    return this;
  }

  /**
   * Add debug information in dumped content, mainly version numbers.
   * Also dump debugging information while reading in object.
   *
   * @return this
   */
  public DumpBuilder enableDebug() {
    dumpAst.getBuildConfig().setDebug(true);
    return thisWithResetBuilt();
  }

  /**
   * Set the name of the package, where all AST classes are located.
   * @param packageName name of the package
   * @return this
   */
  public DumpBuilder setPackageName(String packageName) {
    this.packageName = packageName;
    return thisWithResetBuilt();
  }

  /**
   * Include empty strings for all tokens
   *
   * @return this
   */
  public DumpBuilder includeEmptyStringsOnTokens() {
    dumpAst.getBuildConfig().setIncludeEmptyString(true);
    return thisWithResetBuilt();
  }

  /**
   * Let all relations (and reference attributes) influence layouting of nodes (disabled by default).
   * @return this
   */
  public DumpBuilder enableRelationWithRank() {
    dumpAst.getPrintConfig().setRelationWithRank(true);
    return thisWithResetBuilt();
  }

  // --- Types ---

  /**
   * Disable all objects with types matching at least one of the given regex strings.
   * Disabled objects won't be included in any output. However, their children are still processed.
   * <p>
   * See {@link DumpBuilder} for details on inclusion and exclusion precedence.
   *
   * @param regex first pattern to match type names
   * @param moreRegexes more patterns to match type names
   * @return this
   * @see java.util.regex.Pattern#compile(String)
   */
  public DumpBuilder disableTypes(String regex, String... moreRegexes) {
    updateRegexes(dumpAst.getBuildConfig()::getTypeIgnorePattern,
        dumpAst.getBuildConfig()::setTypeIgnorePattern,
        regex, moreRegexes);
    return thisWithResetBuilt();
  }

  // --- Tokens ---

  public <ASTNODE> DumpBuilder includeToken(IncludeTokenMethod<ASTNODE> spec) {
    dumpAst.getBuildConfig().setIncludeTokenMethod(spec);
    return thisWithResetBuilt();
  }

  // --- Children ---

  public <ASTNODE> DumpBuilder includeChild(IncludeChildMethod<ASTNODE> spec) {
    dumpAst.getBuildConfig().setIncludeChildMethod(spec);
    return thisWithResetBuilt();
  }

  // --- Attributes ---

  public <ASTNODE> DumpBuilder includeAttribute(IncludeAttributeMethod<ASTNODE> spec) {
    dumpAst.getBuildConfig().setIncludeAttributeMethod(spec);
    return thisWithResetBuilt();
  }

  // --- Relations ---

  public <ASTNODE> DumpBuilder includeRelation(IncludeRelationMethod<ASTNODE> spec) {
    dumpAst.getBuildConfig().setIncludeRelationMethod(spec);
    return thisWithResetBuilt();
  }

  // --- Settings ---

  /**
   * Omit children that are <code>null</code> (in a full AST, there are no such nodes).
   *
   * Normally, those nodes are shown.
   * @return this
   */
  public DumpBuilder excludeNullNodes() {
    dumpAst.getBuildConfig().setExcludeNullNodes(true);
    return thisWithResetBuilt();
  }

  /**
   * Include children that are <code>null</code> (in a full AST, there are no such nodes).
   *
   * This is the default.
   * @return this
   */
  public DumpBuilder includeNullNodes() {
    dumpAst.getBuildConfig().setExcludeNullNodes(false);
    return thisWithResetBuilt();
  }

  /**
   * Add custom preamble put before diagram content.
   * @param option arbitrary plantuml content
   * @return this
   */
  public DumpBuilder customPreamble(String option) {
    dumpAst.getPrintConfig().addHeader(new Header(option));
    return thisWithResetBuilt();
  }

  /**
   * Add skin param setting
   * @param setting the setting
   * @param value   value of the setting
   * @return this
   */
  public DumpBuilder skinParam(SkinParamStringSetting setting, String value) {
    customPreamble("skinparam " + setting.toString() + " " + value);
    return thisWithResetBuilt();
  }

  /**
   * Add skin param setting
   * @param setting the setting
   * @param value   value of the setting
   * @return this
   */
  public DumpBuilder skinParam(SkinParamBooleanSetting setting, boolean value) {
    customPreamble("skinparam " + setting.toString() + " " + value);
    return thisWithResetBuilt();
  }

  /**
   * Set the styling definition for all nodes to change appearance, e.g., of background color.
   * @param styleDefinition the new style definition
   * @return this
   * @param <ASTNODE> type of AstNode
   */
  public <ASTNODE> DumpBuilder nodeStyle(NodeStyleDefinition<ASTNODE> styleDefinition) {
    dumpAst.getPrintConfig().setNodeStyleDefinition(styleDefinition);
    return thisWithResetBuilt();
  }

  /**
   * Set the styling definition for all relations to change appearance, e.g., of its label.
   * @param styleDefinition the new style definition
   * @return this
   * @param <ASTNODE> type of AstNode
   */
  public <ASTNODE> DumpBuilder relationStyle(RelationStyleDefinition<ASTNODE> styleDefinition) {
    dumpAst.getPrintConfig().setRelationStyleDefinition(styleDefinition);
    return thisWithResetBuilt();
  }

  /**
   * Set color for computed parts of the AST (default: "blue").
   * @param color some color understood by plantuml
   * @return this
   * @see <a href="https://plantuml.com/en/color">https://plantuml.com/en/color</a>
   */
  public DumpBuilder setComputedColor(String color) {
    dumpAst.getBuildConfig().getStyleInformation().setComputedColor(color);
    return thisWithResetBuilt();
  }

  /**
   * Set scale of diagram.
   * @param value the new scale (default: 1.0).
   * @return this
   */
  public DumpBuilder setScale(double value) {
    dumpAst.getPrintConfig().setScale(value);
    return thisWithResetBuilt();
  }

  /**
   * Enable enforced ordering of children nodes (may not always work due to dynamic placement by plantuml).
   * @return this
   */
  public DumpBuilder orderChildren() {
    dumpAst.getPrintConfig().setOrderChildren(true);
    return thisWithResetBuilt();
  }

  // --- Dump methods ---

  /**
   * Return content as plantuml source code.
   *
   * @return the plantuml source code
   * @throws TransformationException if {@link DumpAst#transform(TransformationTransferInformation, Object) transform} was not successful
   */
  public String dumpAsSource() throws TransformationException {
    return build().toPlantUml();
  }

  /**
   * Write out content as plantuml source code.
   *
   * @param destination path of destination file
   * @throws IOException if an I/O error happened during opening or writing in that file
   * @throws TransformationException if {@link DumpAst#transform(TransformationTransferInformation, Object) transform} was not successful
   */
  public void dumpAsSource(Path destination) throws IOException, TransformationException {
    String content = build().toPlantUml();
    try (Writer writer = Files.newBufferedWriter(destination)) {
      writer.write(content);
    }
  }

  /**
   * Return content as intermediate data structure used by the template engine.
   *
   * @return the intermediate data structure used by the template engine
   * @throws TransformationException if {@link DumpAst#transform(TransformationTransferInformation, Object) transform} was not successful
   */
  public String dumpAsYaml(boolean prependCreationComment) throws TransformationException {
    return build().printYaml(prependCreationComment);
  }

  /**
   * Write out content as intermediate data structure used by the template engine.
   *
   * @param destination path of destination file
   * @param prependCreationComment whether to add the creation date as first line
   * @throws IOException if an I/O error happened during opening or writing in that file
   * @throws TransformationException if {@link DumpAst#transform(TransformationTransferInformation, Object) transform} was not successful
   */
  public void dumpAsYaml(Path destination, boolean prependCreationComment) throws IOException, TransformationException {
    String content = build().printYaml(prependCreationComment);
    try (Writer writer = Files.newBufferedWriter(destination)) {
      writer.write(content);
    }
  }

  /**
   * Write out content as PNG image generated by plantuml.
   *
   * @param os a stream to write to
   * @throws IOException if an I/O error happened during writing in that stream
   * @throws TransformationException if {@link DumpAst#transform(TransformationTransferInformation, Object) transform} was not successful
   */
  public void dumpAsPNG(OutputStream os) throws TransformationException, IOException {
    dumpAs(os, FileFormat.PNG);
  }

  /**
   * Write out content as PNG image generated by plantuml.
   *
   * @param destination path of destination file
   * @throws IOException if an I/O error happened during opening or writing in that file
   * @throws TransformationException if {@link DumpAst#transform(TransformationTransferInformation, Object) transform} was not successful
   */
  public void dumpAsPNG(Path destination) throws IOException, TransformationException {
    dumpAs(destination, FileFormat.PNG);
  }

  /**
   * Write out content as SVG image generated by plantuml.
   *
   * @param os a stream to write to
   * @throws IOException if an I/O error happened during writing in that stream
   * @throws TransformationException if {@link DumpAst#transform(TransformationTransferInformation, Object) transform} was not successful
   */
  public void dumpAsSVG(OutputStream os) throws TransformationException, IOException {
    dumpAs(os, FileFormat.SVG);
  }

  /**
   * Write out content as SVG image generated by plantuml.
   *
   * @param destination path of destination file
   * @throws IOException if an I/O error happened during opening or writing in that file
   * @throws TransformationException if {@link DumpAst#transform(TransformationTransferInformation, Object) transform} was not successful
   */
  public void dumpAsSVG(Path destination) throws IOException, TransformationException {
    dumpAs(destination, FileFormat.SVG);
  }

  /**
   * Write out content as PDF image generated by plantuml.
   *
   * <br>
   * <b>Note:</b> This requires additional dependencies, see <a href="https://plantuml.com/pdf">https://plantuml.com/pdf</a>
   *
   * @param os a stream to write to
   * @throws IOException if an I/O error happened during writing in that stream
   * @throws TransformationException if {@link DumpAst#transform(TransformationTransferInformation, Object) transform} was not successful
   */
  public void dumpAsPDF(OutputStream os) throws TransformationException, IOException {
    dumpAs(os, FileFormat.PDF);
  }

  /**
   * Write out content as PDF generated by plantuml.
   *
   * <br>
   * <b>Note:</b> This requires additional dependencies, see <a href="https://plantuml.com/pdf">https://plantuml.com/pdf</a>
   *
   * @param destination path of destination file
   * @throws IOException if an I/O error happened during opening or writing in that file
   * @throws TransformationException if {@link DumpAst#transform(TransformationTransferInformation, Object) transform} was not successful
   */
  public void dumpAsPDF(Path destination) throws IOException, TransformationException {
    dumpAs(destination, FileFormat.PDF);
  }

  private void dumpAs(Path destination, FileFormat fileFormat) throws IOException, TransformationException {
    String content = build().toPlantUml();
    SourceStringReader reader = new SourceStringReader(content);
    reader.outputImage(Files.newOutputStream(destination), new FileFormatOption(fileFormat));
  }

  private void dumpAs(OutputStream os, FileFormat fileFormat) throws IOException, TransformationException {
    String content = build().toPlantUml();
    SourceStringReader reader = new SourceStringReader(content);
    reader.outputImage(os, new FileFormatOption(fileFormat));
  }

  // --- Helper methods ---

  protected DumpAst build() throws TransformationException {
    if (!built) {
      final String packageNameToUse;
      if (this.packageName != null) {
        packageNameToUse = this.packageName;
      } else {
        if (this.target == null) {
          packageNameToUse = null;
        } else {
          packageNameToUse = this.target.getClass().getPackage().getName();
        }
      }
      dumpAst.setPackageName(packageNameToUse);
      try {
        dumpAst.transform(new TransformationTransferInformation(), this.target);
      } catch (InvocationTargetException | IllegalAccessException | NoSuchMethodException e) {
        dumpAst = null;
        throw new TransformationException(e);
      }
    }
    return dumpAst;
  }

  private void updateRegexes(Supplier<String> getter, Consumer<String> setter, String regex, String... moreRegexes) {
    if (getter.get().isEmpty()) {
      setter.accept(regex);
    } else {
      setter.accept(getter.get() + "|" + regex);
    }
    for (String value : moreRegexes) {
      setter.accept(getter.get() + "|" + value);
    }
  }

  private String readVersion() {
    try {
      ResourceBundle resources = ResourceBundle.getBundle("dumpAstVersion");
      return resources.getString("version");
    } catch (java.util.MissingResourceException e) {
      return "version ?";
    }
  }
}
