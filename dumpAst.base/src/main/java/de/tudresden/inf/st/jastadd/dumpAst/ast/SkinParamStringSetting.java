package de.tudresden.inf.st.jastadd.dumpAst.ast;

public enum SkinParamStringSetting {
  /**
   * Set color of background
   */
  backgroundColor
}
