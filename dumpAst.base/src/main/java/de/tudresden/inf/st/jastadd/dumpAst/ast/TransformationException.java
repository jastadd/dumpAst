package de.tudresden.inf.st.jastadd.dumpAst.ast;

/**
 * Exception when transformation was unsuccessful.
 *
 * @author rschoene - Initial contribution
 */
public class TransformationException extends Exception {
  public TransformationException(Throwable cause) {
    super("Could not transform :(", cause);
  }
}
