package de.tudresden.inf.st.jastadd.dumpAst.ast;

/**
 * Entrypoint for dumpAst.
 *
 * @author rschoene - Initial contribution
 */
public class Dumper {
  /**
   * Prepare to read in the given object. Use the <code>dump*</code> methods to actually dump its content.
   * @param obj the object to dump
   * @return a builder to adjust dump options
   */
  public static DumpBuilder read(Object obj) {
    return new DumpBuilder(obj);
  }
}
