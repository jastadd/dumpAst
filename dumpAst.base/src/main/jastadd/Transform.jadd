aspect Transform {
  class DumpAst {
    enum Source {
      ROOT, NORMAL, RELATION
    }
    class TransformationOptions {
      // todo should be read-only, i.e., copy-on-write
      public Source source;
      public boolean invisible;
      public boolean allowNullObjects;
      public boolean computed;

      public TransformationOptions asRelation() {
        return fromSource(Source.RELATION, false).computed(false).allowNullObjectsOnce();
      }

      public TransformationOptions asRoot() {
        return fromSource(Source.ROOT, false).allowNullObjectsOnce();
      }

      public TransformationOptions asNormal(boolean shouldBeInvisible) {
        return fromSource(Source.NORMAL, shouldBeInvisible);
      }

      public TransformationOptions computed(boolean computed) {
        if (computed == this.computed) {
          return this;
        }
        return new TransformationOptions(this.source, this.invisible, this.allowNullObjects, computed);
      }

      public TransformationOptions allowNullObjectsOnce() {
        return new TransformationOptions(this.source, this.invisible, !DumpAst.this.getBuildConfig().getExcludeNullNodes(), this.computed);
      }

      private TransformationOptions fromSource(Source source, boolean shouldBeInvisible) {
        return new TransformationOptions(source, this.invisible || shouldBeInvisible, this.computed);
      }

      private TransformationOptions(Source source, boolean invisible, boolean computed) {
        this(source, invisible, false, computed);
      }
      private TransformationOptions(Source source, boolean invisible, boolean allowNullObjects, boolean computed) {
        this.source = source;
        this.invisible = invisible;
        this.allowNullObjects = allowNullObjects;
        this.computed = computed;
      }
    }
  }

  private TransformationOptions DumpAst.options() {
    return new TransformationOptions(Source.NORMAL, false, false);
  }

  public static final Object DumpAst.EMPTY = new Object();

  // --- transform --- (need to be a method, because it alters the AST while traversing the object structure)
  protected DumpNode DumpAst.transform(TransformationTransferInformation tti, Object obj)
      throws java.lang.reflect.InvocationTargetException, IllegalAccessException, NoSuchMethodException {
    DumpNode result = transform(tti, obj, options().asRoot());
    setRootNode(result);
    // post-process relationTargetsUnprocessed
    boolean someAreUnprocessed = true;
    while (someAreUnprocessed) {
      someAreUnprocessed = false;
      java.util.Map<DumpNode, Boolean> copy = new java.util.HashMap<>(tti.relationTargetsUnprocessed);
      for (java.util.Map.Entry<DumpNode, Boolean> entry : copy.entrySet()) {
        if (entry.getValue()) {
          transform(tti, entry.getKey().getObject(), options().asRoot());
          someAreUnprocessed = true;
        }
      }
    }
    return result;
  }

  protected DumpNode DumpAst.transform(
      TransformationTransferInformation tti, Object obj, TransformationOptions options)
      throws java.lang.reflect.InvocationTargetException, IllegalAccessException, NoSuchMethodException {
    if (obj == null && !options.allowNullObjects) {
      return null;
    }
    DumpNode node;
    String objClassName;
    if (obj == null || obj == EMPTY) {
      node = null;
      objClassName = "null";
    } else {
      node = tti.transformed.get(obj);
      objClassName = obj.getClass().getSimpleName();
    }
    if (node != null) {
      if (options.source == Source.RELATION) {
        return node;
      }
      // either processing as parent, or later as root-node. so mark it as processed.
      tti.relationTargetsUnprocessed.put(node, false);
    } else {
      node = new DumpNode();
      node.setObject(obj);
      node.setName(tti.nextName());
      node.setComputed(options.computed);
      tti.transformed.put(obj, node);
      this.addDumpNode(node);
    }
    if (!node.isAstNode()) {
      return node;
    }
    applyStyle(node);
    // do not process node further if coming from a relation
    if (options.source == Source.RELATION) {
      tti.relationTargetsUnprocessed.put(node, true);
      return node;
    }
    if (options.invisible || !isTypeEnabled(objClassName)) {
      node.setInvisible(true);
    }
    if (obj == null || obj == EMPTY) {
      // for a null or empty object, we do not need any further analysis
      return node;
    }
    final ClassAnalysisResult car = analyzeClass(obj.getClass());
    if (getBuildConfig().getDebug()) {
      System.out.println("for node " + obj + ", analysis was:\n" + car.prettyPrint());
    }
    for (AnalysedMethod containmentMethod : car.getContainmentMethodList()) {
      if (containmentMethod.isSingleChildMethod() || containmentMethod.isOptChildMethod()) {
        handleContainmentSingleOrOptChild(node, containmentMethod, obj, tti, options);
      } else if (containmentMethod.isListChildMethod()) {
        handleContainmentListChild(node, containmentMethod, obj, tti, options);
      } else {
        throw new RuntimeException("Unknown containment method type " + containmentMethod);
      }
    }
    for (AnalysedMethod otherMethod : car.getOtherMethodList()) {
      if (otherMethod.isSingleChildMethod()) {
        handleOtherSingleChild(node, otherMethod, obj, tti, options);
      } else if (otherMethod.isListChildMethod()) {
        handleOtherListChild(node, otherMethod, obj, tti, options);
      } else if (otherMethod.isSingleRelationMethod()) {
        handleOtherSingleRelation(node, otherMethod, obj, tti, options);
      } else if (otherMethod.isListRelationMethod()) {
        handleOtherListRelation(node, otherMethod, obj, tti, options);
      } else if (otherMethod.isTokenMethod()) {
        handleOtherToken(node, otherMethod, obj, tti, options);
      } else {
        throw new RuntimeException("Unknown other method type " + otherMethod);
      }
    }
    return node;
  }

  private boolean DumpAst.handleContainmentSingleOrOptChild(DumpNode node, AnalysedMethod containmentMethod,
      Object obj, TransformationTransferInformation tti, TransformationOptions options)
      throws java.lang.reflect.InvocationTargetException, IllegalAccessException, NoSuchMethodException {
    // -- singleChild or optChild --
    Object target;
    if (containmentMethod.isOptChildMethod() && !((boolean) containmentMethod.asOptChildMethod().getCheckMethod().invoke(obj))) {
      if (getBuildConfig().getExcludeNullNodes()) {
        return false;
      } else {
        target = null;
      }
    } else {
      target = containmentMethod.getMethod().invoke(obj);
    }
    String childName = containmentMethod.getName();
    if (!getBuildConfig().getIncludeChildMethod().shouldInclude(obj, target, childName)) {
      return false;
    }
    DumpNode targetNode = transform(tti, target, options.asNormal(false).allowNullObjectsOnce());
    if (targetNode != null) {
      DumpNormalChildNode normalChild = new DumpNormalChildNode();
      normalChild.setLabel(childName + (containmentMethod.isOptChildMethod() ? "?" : ""));
      normalChild.setDumpNode(targetNode);
      normalChild.setComputed(false);
      node.addDumpChildNode(normalChild);
      applyStyle(normalChild);
    }
    return true;
  }

  private boolean DumpAst.handleContainmentListChild(DumpNode node, AnalysedMethod containmentMethod,
  Object obj, TransformationTransferInformation tti, TransformationOptions options)
      throws java.lang.reflect.InvocationTargetException, IllegalAccessException, NoSuchMethodException {
    // -- listChild --
    Iterable<?> targetList = (Iterable<?>) containmentMethod.getMethod().invoke(obj);
    DumpListChildNode listChild = new DumpListChildNode();
    listChild.setComputed(false);
    String childName = containmentMethod.getName();
    listChild.setLabel(childName);
    listChild.setName(tti.nextName());
    node.addDumpChildNode(listChild);
    RelationStyle style = applyStyle(listChild);

    int index = -1;
    for (Object target : targetList) {
      index++;
      if (!getBuildConfig().getIncludeChildMethod().shouldInclude(obj, target, childName)) {
        continue;
      }
      DumpNode targetNode = transform(tti, target, options.asNormal(false));
      if (target != null && targetNode != null) {
        InnerDumpNode inner = new InnerDumpNode().setDumpNode(targetNode).setOriginalIndex(index);
        listChild.addInnerDumpNode(inner);
        inner.applyStyle(style);
      }
    }
    if (listChild.getNumInnerDumpNode() == 0) {
      InnerDumpNode inner = new InnerDumpNode().setDumpNode(transform(tti, EMPTY,
          options.asNormal(false).allowNullObjectsOnce()));
      listChild.addInnerDumpNode(inner);
      inner.applyStyle(style);
    }
    return true;
  }

  private boolean DumpAst.handleOtherSingleChild(DumpNode node, AnalysedMethod otherMethod,
      Object obj, TransformationTransferInformation tti, TransformationOptions options)
      throws java.lang.reflect.InvocationTargetException, IllegalAccessException, NoSuchMethodException {
    // -- singleChild --
    String childName = otherMethod.getName();
    boolean computed = otherMethod.asSingleChildMethod().isNTASingleChildMethod();
    boolean shouldInclude = computed ?
        getBuildConfig().getIncludeAttributeMethod().shouldInclude(obj, childName, true, () -> catchedInvoke(otherMethod.getMethod(), obj)) :
        getBuildConfig().getIncludeChildMethod().shouldInclude(obj, otherMethod.getMethod().invoke(obj), childName);
    if (!shouldInclude) {
      return false;
    }
    DumpNode targetNode = transform(tti, otherMethod.getMethod().invoke(obj), options.asNormal(false).computed(computed).allowNullObjectsOnce());
    if (targetNode != null) {
      DumpNormalChildNode normalChild = new DumpNormalChildNode();
      normalChild.setLabel(childName);
      normalChild.setDumpNode(targetNode);
      normalChild.setComputed(computed);
      node.addDumpChildNode(normalChild);
      applyStyle(normalChild);
    }
    return true;
  }

  private boolean DumpAst.handleOtherListChild(DumpNode node, AnalysedMethod otherMethod,
      Object obj, TransformationTransferInformation tti, TransformationOptions options)
      throws java.lang.reflect.InvocationTargetException, IllegalAccessException, NoSuchMethodException {
    // -- listChild --
    // it is always a NTAListChildMethod
    String childName = otherMethod.getName();
    if (!getBuildConfig().getIncludeAttributeMethod().shouldInclude(obj, childName, true, () -> catchedInvoke(otherMethod.getMethod(), obj))) {
      return false;
    }
    Iterable<?> targetList = (Iterable<?>) otherMethod.getMethod().invoke(obj);
    DumpListChildNode listChild = new DumpListChildNode();
    boolean computed = otherMethod.asListChildMethod().isNTAListChildMethod();
    listChild.setComputed(computed);
    listChild.setLabel(childName);
    listChild.setName(tti.nextName());
    node.addDumpChildNode(listChild);
    RelationStyle style = applyStyle(listChild);

    for (Object target : targetList) {
      DumpNode targetNode = transform(tti, target, options.asNormal(false).computed(computed));
      if (target != null && targetNode != null) {
        InnerDumpNode inner = new InnerDumpNode().setDumpNode(targetNode);
        listChild.addInnerDumpNode(inner);
        inner.applyStyle(style);
      }
    }
    return true;
  }

  private boolean DumpAst.handleOtherSingleRelation(DumpNode node, AnalysedMethod otherMethod,
      Object obj, TransformationTransferInformation tti, TransformationOptions options)
      throws java.lang.reflect.InvocationTargetException, IllegalAccessException, NoSuchMethodException {
    // -- singleRelation --
    Object target = otherMethod.getMethod().invoke(obj);
    if (!getBuildConfig().getIncludeRelationMethod().shouldInclude(obj, target, otherMethod.getName())) {
      return false;
    }
    DumpNode targetNode = transform(tti, target, options.asRelation());
    if (targetNode != null) {
      DumpNormalRelation normalRelation = new DumpNormalRelation();
      normalRelation.setLabel(otherMethod.getName() +
          (otherMethod.asSingleRelationMethod().isOptRelationMethod() ? "?" : ""));
      normalRelation.setDumpNode(targetNode);
      node.addDumpRelation(normalRelation);
      applyStyle(normalRelation);
    }
    return true;
  }

  private boolean DumpAst.handleOtherListRelation(DumpNode node, AnalysedMethod otherMethod,
      Object obj, TransformationTransferInformation tti, TransformationOptions options)
      throws java.lang.reflect.InvocationTargetException, IllegalAccessException, NoSuchMethodException {
    // -- listRelation --
    Iterable<?> targetList = (Iterable<?>) otherMethod.getMethod().invoke(obj);
    DumpListRelation listRelation = new DumpListRelation();
    listRelation.setLabel(otherMethod.getName());
    listRelation.setName(tti.nextName());
    node.addDumpRelation(listRelation);
    RelationStyle style = applyStyle(listRelation);

    int index = -1;
    for (Object target : targetList) {
      index++;
      if (!getBuildConfig().getIncludeRelationMethod().shouldInclude(obj, target, otherMethod.getName())) {
        continue;
      }
      DumpNode targetNode = transform(tti, target, options.asRelation());
      if (target != null && targetNode != null) {
        InnerRelationDumpNode inner = new InnerRelationDumpNode().setDumpNode(targetNode).setOriginalIndex(index);
        listRelation.addInnerRelationDumpNode(inner);
        inner.applyStyle(style);
      }
    }
    if (listRelation.getNumInnerRelationDumpNode() == 0) {
      InnerRelationDumpNode inner = new InnerRelationDumpNode().setDumpNode(transform(tti, EMPTY,
          options.asNormal(false).allowNullObjectsOnce()));
      listRelation.addInnerRelationDumpNode(inner);
      inner.applyStyle(style);
    }
    return true;
  }

  private boolean DumpAst.handleOtherToken(DumpNode node, AnalysedMethod otherMethod,
      Object obj, TransformationTransferInformation tti, TransformationOptions options)
      throws java.lang.reflect.InvocationTargetException, IllegalAccessException, NoSuchMethodException {
    // -- token --
    TokenMethod tokenMethod = otherMethod.asTokenMethod();
    boolean shouldInclude = tokenMethod.isAttributeMethod() ?
        getBuildConfig().getIncludeAttributeMethod().shouldInclude(obj, otherMethod.getName(), false, () -> catchedInvoke(otherMethod.getMethod(), obj)) :
        getBuildConfig().getIncludeTokenMethod().shouldInclude(obj, otherMethod.getName(), otherMethod.getMethod().invoke(obj));
    if (!shouldInclude) {
      return false;
    }
    Object target = otherMethod.getMethod().invoke(obj);
    // local function to add common member and add the token-node to the given node
    java.util.function.Consumer<DumpToken> setMemberAndAdd = token -> {
      token.setLabel(otherMethod.getName());
      token.setComputed(otherMethod.asTokenMethod().isAttributeMethod());
      node.addDumpToken(token);
    };
    if (target != null) {
      boolean atLeastOneASTNode = false;
      if (Iterable.class.isAssignableFrom(target.getClass())) {
        java.util.List<DumpNode> nodes = new java.util.ArrayList<>();
        Iterable<?> iterable = (Iterable<?>) target;
        for (Object element : iterable) {
          // check if isAstNode for first non-null. if yes, use DumpReferenceListToken, otherwise DumpValueToken
          DumpNode nodeForElement = transform(tti, element, options.asRelation());
          nodes.add(nodeForElement);
          if (nodeForElement != null && nodeForElement.isAstNode()) {
            atLeastOneASTNode = true;
          }
        }
        if (atLeastOneASTNode) {
          DumpReferenceListToken listToken = new DumpReferenceListToken();
          listToken.setName(tti.nextName());
          setMemberAndAdd.accept(listToken);
          RelationStyle style = applyStyle(listToken);
          nodes.forEach(element -> {
            InnerRelationDumpNode inner = new InnerRelationDumpNode().setDumpNode(element);
            listToken.addInnerRelationDumpNode(inner);
            inner.applyStyle(style);
          });
        }
      }
      if (!atLeastOneASTNode) {
        DumpNode targetNode = transform(tti, target, options.asRelation());
        if (targetNode != null && targetNode.isAstNode()) {
          DumpReferenceToken token = new DumpReferenceToken().setDumpNode(targetNode);
          setMemberAndAdd.accept(token);
          applyStyle(token.asDumpReferenceToken());
        } else {
          if (getBuildConfig().getIncludeEmptyString() || !target.toString().isEmpty()) {
            DumpValueToken valueToken = new DumpValueToken();
            valueToken.setValue(target);
            setMemberAndAdd.accept(valueToken);
          }
        }
      }
    }
    return true;
  }

  private Object DumpAst.catchedInvoke(java.lang.reflect.Method method, Object obj) {
    try {
      return method.invoke(obj);
    }
    catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  // === NodeStyle ===
  DumpNode NodeStyle.node;

  NodeStyle DumpNode.createDefaultStyle() {
    NodeStyle result = new NodeStyle();
    result.node = this;
    result.useSimpleLabelWithHashCode();
    return result;
  }

  private void DumpAst.applyStyle(DumpNode node) {
    NodeStyle style = node.createDefaultStyle();
    getPrintConfig().getNodeStyleDefinition().style(node.getObject(), style);
    node.setLabel(style.getLabel());
    node.setBackgroundColor(style.getBackgroundColor());
    node.setTextColor(style.getTextColor());
    node.setManualStereotypes(style.getStereoTypes());
  }

  // === RelationStyle ===
  interface RelationStyleDefinable {
    // more methods in TransformPlus.jadd

    // attributes
    BuildConfig buildConfig();
    boolean isComputed();
    boolean isContainment();
    String initialLabel();
    DumpNode containingDumpNode();
    DumpNode getDumpNode();
  }
  interface RelationStylable<SELF> {
    // more methods in TransformPlus.jadd

    // tokens
    String getTextColor();
    String getLineColor();

    SELF setLabel(String name);
    SELF setTextColor(String textColor);
    SELF setLineColor(String lineColor);
  }

  DumpChildNode implements RelationStyleDefinable, RelationStylable<DumpChildNode>;
  syn String DumpChildNode.initialLabel() = getLabel();
  syn boolean DumpChildNode.isComputed() = getComputed();
  syn boolean DumpChildNode.isContainment() = true;

  syn DumpNode DumpListChildNode.getDumpNode() = null;

  DumpRelation implements RelationStyleDefinable, RelationStylable<DumpRelation>;
  syn String DumpRelation.initialLabel() = getLabel();
  syn boolean DumpRelation.isComputed() = false;
  syn boolean DumpRelation.isContainment() = false;

  syn DumpNode DumpListRelation.getDumpNode() = null;

  InnerDumpNode implements RelationStylable<InnerDumpNode>;
  InnerRelationDumpNode implements RelationStylable<InnerRelationDumpNode>;

  DumpReferenceListToken implements RelationStyleDefinable, RelationStylable<DumpReferenceListToken>;
  syn String DumpReferenceListToken.initialLabel() = getLabel();
  syn boolean DumpReferenceListToken.isComputed() = getComputed();
  syn boolean DumpReferenceListToken.isContainment() = false;
  syn DumpNode DumpReferenceListToken.getDumpNode() = null;

  DumpReferenceToken implements RelationStyleDefinable, RelationStylable<DumpReferenceToken>;
  syn String DumpReferenceToken.initialLabel() = getLabel();
  syn boolean DumpReferenceToken.isComputed() = getComputed();
  syn boolean DumpReferenceToken.isContainment() = false;

  private void DumpAst.applyStyle(RelationStylable<?> stylable, RelationStyle style) {
    stylable.applyStyle(style);
  }
  private void DumpAst.applyStyle(DumpToken token, RelationStyle style) {
    if (token.isDumpReferenceListToken()) {
      token.asDumpReferenceListToken().applyStyle(style);
    } else if (token.isDumpReferenceToken()) {
      token.asDumpReferenceToken().applyStyle(style);
    }
    // otherwise ignore
  }

  // --- isTypeEnabled ---
  syn boolean DumpAst.isTypeEnabled(String typeName) {
    return !getBuildConfig().typeIgnorePattern().matcher(typeName).matches();
  }
  // --- typeIgnorePattern ---
  syn java.util.regex.Pattern BuildConfig.typeIgnorePattern() = java.util.regex.Pattern.compile(getTypeIgnorePattern());

  class TransformationTransferInformation {
    java.util.Map<Object, DumpNode> transformed = new java.util.HashMap<>();
    java.util.Map<DumpNode, Boolean> relationTargetsUnprocessed = new java.util.HashMap<>();
    int nodeCounter = 0;

    String nextName() {
      return "node" + (nodeCounter++);
    }
  }
}
