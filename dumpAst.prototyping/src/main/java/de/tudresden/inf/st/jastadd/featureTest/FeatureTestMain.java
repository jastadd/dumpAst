package de.tudresden.inf.st.jastadd.featureTest;

import de.tudresden.inf.st.jastadd.dumpAst.ast.DumpBuilder;
import de.tudresden.inf.st.jastadd.dumpAst.ast.Dumper;
import de.tudresden.inf.st.jastadd.dumpAst.ast.SkinParamBooleanSetting;
import de.tudresden.inf.st.jastadd.dumpAst.ast.TransformationException;
import org.jastadd.featureTest.ast.*;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

/**
 * Main class of feature test.
 *
 * @author rschoene - Initial contribution
 */
public class FeatureTestMain {

  public static void main(String[] args) throws IOException, TransformationException {
    new FeatureTestMain().run(args);
  }

  private void run(String[] args) throws TransformationException, IOException {
    String mode = args.length == 0 ? "default" : args[0];
    System.out.println(Arrays.toString(args) + " -> " + mode);

    final DumpBuilder builder;

    switch (mode) {
      case "default":
      case "withRoot":
        builder = withRoot();
        break;
      case "withOtherRoot":
        builder = withOtherRoot();
        break;
      default:
        throw new UnsupportedOperationException("Unknown mode: " + mode);
    }

    Path pathToYaml = Paths.get("featureTest.yml");
    Path pathToPng = Paths.get("featureTest.png");
    Path pathToSvg = Paths.get("featureTest.svg");

    builder.dumpAsYaml(pathToYaml, true);
    builder.dumpAsPNG(pathToPng);
    builder.dumpAsSVG(pathToSvg);
    builder.dumpAsSource(Paths.get("featureTest.puml"));
  }

  private DumpBuilder withRoot() {
    Root root = new Root();
    root.setName("Root1");
    A a = new A().setName("A2");
    a.setB(new B().setName("B2.1"));
    a.setD(new D());
//    a.setMyC(new C().setName("C2.1"));
    B b1 = new B().setName("B3").setOtherValue("some long text");
//    C c = new C().setName("C4");
//    c.setA(new A().setName("A4.1").setB(new B().setName("B4.1.1")));
//    c.setRawReference(a);
//    b1.setOneA(a);
    B b2 = new B().setName("B5").setOtherValue("#ff00ff");
//    C myC = new C().setName("C6");
//    c.setA(new A().setName("A6.1").setB(new B().setName("B6.1.1")));
//    a.setMyC(myC);
    root.setA(a);
    root.addB(b1);
    root.addB(b2);
//    root.setC(c);

    return Dumper
//        .read(null)
        .read(root)
//        .enableRelationWithRank()
//        .customPreamble("hide empty members")
        .enableDebug()
        .customPreamble("title My fancy title")
        .includeChild((parentNode, childNode, contextName) -> {
//          if (parentNode instanceof A && ((A) parentNode).getName().equals("A2")) {
//            return false;
//          }
          if (parentNode instanceof Root && childNode instanceof B) {
            return !"B3".equals(((B) childNode).getName());
          }
//          return !contextName.equals("MyC");
          return true;
        })
        .includeRelation((sourceNode, targetNode, roleName) ->
            !(sourceNode instanceof B) || !((B) sourceNode).getName().equals("B6.1.1"))
        .includeAttribute((node, attributeName, isNTA, supplier) -> {
          switch (attributeName) {
            case "referenceAttr":
            case "collectBs":
            case "Calculated":
            case "AlsoCalculatedListNewSyntax":
            case "AAA":
            case "fancyName":
              return true;
            default:
              return false;
          }
        })
        .skinParam(SkinParamBooleanSetting.Shadowing, false)
        .relationStyle((source, target, isComputed, isContainment, style) -> {
          System.out.println(style.getLabel() + ", computed: " + isComputed + ", containment: " + isContainment + ", textC: " + style.getTextColor() + ", lineC: " + style.getLineColor());
          if (isContainment && target != null && style.getLabel().equals(target.getClass().getSimpleName())) {
            style.setLabel("");
          }
          switch (style.getLabel()) {
            case "ManyA":
              style.setLabel("ManyA of " + ((Nameable) source).getName());
              break;
            case "OneA":
            case "MaybeC?":
              style.setTextColor("red");
              style.setLineColor("green");
              break;
            case "B":
              style.setLineColor("orange");
              style.setTextColor("orange");
              break;
            case "collectBs":
              style.setLineColor("purple");
              style.setTextColor("purple");
              break;
          }
        })
        .<ASTNode<?>>nodeStyle((node, style) -> {
          if (node.isA()) {
            style.setBackgroundColor("yellow");
          }
          if (node instanceof B && ((B) node).getOtherValue().startsWith("#")) {
            style.setBackgroundColor(((B) node).getOtherValue().substring(1));
          }
          style.setLabel(node.getClass().getSimpleName() + ASTNode.counter++);
        });
  }

  private DumpBuilder withOtherRoot() {
    OtherRoot root = new OtherRoot();
    E e1 = new E().setName("E1");
    E e2 = new E().setName("E2");
    E e3 = new E().setName("E3");
    E e4 = new E().setName("E4");
    F f1 = new F().setName("F1");
    F f2 = new F().setName("F2");
    F f3 = new F().setName("F3");
    F f4 = new F().setName("F4");
    for (E e : new E[]{e1, e2, e3, e4}) {
      root.addE(e);
    }
    for (F f : new F[]{f1, f2, f3, f4}) {
      root.addF(f);
    }
    e1.addMyF(f1);
    e1.addMyF(f2);
    e2.addMyF(f2);
    e3.addMyF(f2);

    return Dumper.read(root)
        .includeToken((node, tokenName, value) -> false)
        .nodeStyle((node, style) -> {
          style.setLabel(((Nameable) node).getName());
        });
  }
}
